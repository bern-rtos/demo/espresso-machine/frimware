#![allow(unused)]

/// ISSI IS42S16160G SDRAM
/// source: https://github.com/stm32-rs/stm32-fmc/blob/master/src/devices/is42s16400j.rs

use stm32_fmc::{SdramChip, SdramConfiguration, SdramTiming};


const BURST_LENGTH_1: u16 = 0x0000;
const BURST_LENGTH_2: u16 = 0x0001;
const BURST_LENGTH_4: u16 = 0x0002;
const BURST_LENGTH_8: u16 = 0x0004;
const BURST_TYPE_SEQUENTIAL: u16 = 0x0000;
const BURST_TYPE_INTERLEAVED: u16 = 0x0008;
const CAS_LATENCY_2: u16 = 0x0020;
const CAS_LATENCY_3: u16 = 0x0030;
const OPERATING_MODE_STANDARD: u16 = 0x0000;
const WRITEBURST_MODE_PROGRAMMED: u16 = 0x0000;
const WRITEBURST_MODE_SINGLE: u16 = 0x0200;

/// Is42s16160g with Speed Grade 7
///
/// Configured with CAS latency 3, limited 90MHz
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Is42s16160g {}

impl SdramChip for Is42s16160g {
    /// Value of the mode register
    const MODE_REGISTER: u16 = BURST_LENGTH_1
        | BURST_TYPE_SEQUENTIAL
        | CAS_LATENCY_3
        | OPERATING_MODE_STANDARD
        | WRITEBURST_MODE_SINGLE;

    /// Timing Parameters
    const TIMING: SdramTiming = SdramTiming {
        startup_delay_ns: 100_000,    // 100 µs
        max_sd_clock_hz: 90_000_000,  // 90 MHz
        refresh_period_ns: 7_812,    // 64ms / (8192 rows) = 7812ns
        mode_register_to_active: 2,   // tMRD = 2 cycles
        exit_self_refresh: 7,         // tXSR = 70ns
        active_to_precharge: 4,       // tRAS = 37ns
        row_cycle: 7,                 // tRC = 60ns
        row_precharge: 3,             // tRP = 15ns
        row_to_column: 3,             // tRCD = 15ns
    };

    /// SDRAM controller configuration
    const CONFIG: SdramConfiguration = SdramConfiguration {
        column_bits: 9,
        row_bits: 13,
        memory_data_width: 16,
        internal_banks: 4,
        cas_latency: 3,
        write_protection: false,
        read_burst: true,
        read_pipe_delay_cycles: 0,
    };
}
