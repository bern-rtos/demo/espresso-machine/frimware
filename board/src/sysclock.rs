use core::sync::atomic::{AtomicU32, Ordering};
use cortex_m::peripheral::SYST;
use cortex_m::peripheral::syst::SystClkSource;
use cortex_m_rt::exception;
use embedded_time::{self as time};

static TICK: AtomicU32 = AtomicU32::new(0);

#[derive(Copy, Clone)]
pub struct SysClock {}

impl time::Clock for SysClock {
    type T = u32;
    const SCALING_FACTOR: time::fraction::Fraction = <time::fraction::Fraction>::new(1, 1_000);

    fn try_now(&self) -> Result<time::Instant<Self>, time::clock::Error> {
        Ok(time::Instant::new(TICK.load(Ordering::Relaxed)))
    }
}

impl SysClock {
    pub fn new(mut systick: SYST) -> SysClock {
        systick.set_clock_source(SystClkSource::Core);
        systick.set_reload(super::SYSCLK.to_kHz() - 1);
        systick.clear_current();

        systick.enable_counter();
        systick.enable_interrupt();

        SysClock { }
    }

    pub fn ticks(&self) -> u32 {
        TICK.load(Ordering::Relaxed)
    }
}

#[allow(non_snake_case)]
#[exception]
fn SysTick() {
    TICK.fetch_add(1, Ordering::Relaxed);
}