use stm32f7xx_hal::ltdc::DisplayConfig;

pub const RIVERDI_RVT50HQTNWC00: DisplayConfig = DisplayConfig {
    active_width: 800,
    active_height: 480,
    h_back_porch: 8,
    h_front_porch: 8,
    h_sync: 4,
    v_back_porch: 8,
    v_front_porch: 8,
    v_sync: 4,
    frame_rate: 30,
    h_sync_pol: false,
    v_sync_pol: false,
    no_data_enable_pol: false,
    pixel_clock_pol: false,
};