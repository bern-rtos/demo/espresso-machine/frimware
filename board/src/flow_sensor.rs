use core::cell::UnsafeCell;
use core::sync::atomic::{AtomicU32, Ordering};
use stm32f7xx_hal::gpio::{EPin, Input, PullUp, ExtiPin};

const INC_TO_ML_FACTOR: f32 = 1.4;

pub struct Pins {
    pub flow: EPin<Input<PullUp>>,
}

pub struct FlowSensor {
    count: AtomicU32,
    pin: UnsafeCell<EPin<Input<PullUp>>>,
}


impl FlowSensor {
    pub fn new(pins: Pins) -> Self {
        FlowSensor {
            count: AtomicU32::new(0),
            pin: UnsafeCell::new(pins.flow)
        }
    }

    pub fn reset(&self) {
        self.count.store(0, Ordering::Relaxed);
    }

    /// Returns the volume since the last reset in ml.
    pub fn volume(&self) -> f32 {
        self.count.load(Ordering::Relaxed) as f32 * INC_TO_ML_FACTOR
    }

    pub fn inc_update(&self) {
        if unsafe { (*self.pin.get()).is_low() } {
            self.count.fetch_add(1, Ordering::Relaxed);
        }
        unsafe { (*self.pin.get()).clear_interrupt_pending_bit(); }
    }
}

unsafe impl Sync for FlowSensor {}

pub fn irqn() -> u16 {
    stm32f7xx_hal::interrupt::EXTI9_5 as u16
}