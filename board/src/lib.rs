#![no_std]

pub mod actuators;
pub mod buttons;
pub mod analog_sensors;
pub mod flow_sensor;
pub mod sense_bus;
mod clock;
mod fmc;
pub mod state;
#[cfg(feature = "ethernet")]
pub mod ethernet;
pub mod traits;
pub mod display;
#[cfg(feature = "sysclock")]
pub mod sysclock;

use cortex_m::peripheral::NVIC;
use stm32f7xx_hal::pac::TIM1;
// re-export HAL
pub use stm32f7xx_hal as hal;

pub use stm32f7xx_hal;
use stm32f7xx_hal::pac::Peripherals;
use stm32f7xx_hal::prelude::*;
use stm32f7xx_hal::rcc::{HSEClock, HSEClockMode, MCO1};
use stm32f7xx_hal::gpio::{Edge, Speed};
use stm32f7xx_hal::timer::Delay;
use stm32f7xx_hal::gpio::ExtiPin;

pub type BoardDelay = Delay<TIM1, 1_000_000>;
pub const SYSCLK: fugit::Hertz<u32> = fugit::Hertz::<u32>::MHz(216);

pub struct Board {
    pub buttons: buttons::Buttons,
    pub state: state::State,
    pub actuators: actuators::Actuators,
    pub analog_sensors: analog_sensors::AnalogSensors,
    pub sense_bus: sense_bus::SenseBus,
    pub flow_sensor: flow_sensor::FlowSensor,
    pub display: display::Display,
    #[cfg(feature = "ethernet")]
    pub ethernet: ethernet::Ethernet,
    pub delay: BoardDelay,
    #[cfg(feature = "sysclock")]
    pub sys_clock: sysclock::SysClock,
}


impl Board {
    pub fn new() -> Self {
        let mut p = Peripherals::take()
            .expect("cannot take stm32 peripherals");
        let mut cp = cortex_m::Peripherals::take()
            .expect("cannot take core peripherals");
        cp.SCB.disable_dcache(&mut cp.CPUID);

        // system clock
        let mut rcc = p.RCC.constrain();
        let clocks = rcc.cfgr
            .hse(HSEClock::new(25.MHz(), HSEClockMode::Oscillator))
            .sysclk(SYSCLK)
            .hclk(216.MHz())
            .mco1(MCO1::Hse)
            .freeze();

        let mut delay = p.TIM1.delay(&clocks);

        // GPIO ports
        let gpioa = p.GPIOA.split();
        let gpiob = p.GPIOB.split();
        let gpioc = p.GPIOC.split();
        let gpiod = p.GPIOD.split();
        let gpioe = p.GPIOE.split();
        let gpiof = p.GPIOF.split();
        let gpiog = p.GPIOG.split();
        let gpioh = p.GPIOH.split();
        let gpioi = p.GPIOI.split();
        let gpioj = p.GPIOJ.split();
        let gpiok = p.GPIOK.split();


        let buttons = buttons::Buttons::new(buttons::Pins {
            brew: gpioa.pa11.into_pull_up_input().erase(),
            water: gpioc.pc11.into_pull_up_input().erase(),
            steam: gpioc.pc9.into_pull_up_input().erase(),
        });

        let state = state::State::new(state::Pins {
            ok: gpioh.ph8.into_push_pull_output().erase(),
            error: gpiob.pb14.into_push_pull_output().erase(),
            comm: gpiob.pb15.into_push_pull_output().erase()
        });

        let actuators = actuators::Actuators::new(actuators::Pins {
            pump: gpioi.pi7.into_push_pull_output().erase(),
            valve: gpioc.pc7.into_push_pull_output().erase(),
            user: gpioc.pc6.into_push_pull_output().erase(),
            boiler: gpioh.ph14.into_push_pull_output().erase(),
            zero_crossing: gpioh.ph13.into_pull_up_input().erase(),
        });

        // ADC
        let pressure_pin = gpiob.pb1.into_analog();
        let analog_sensors = analog_sensors::AnalogSensors::new(
            analog_sensors::Pins{
                pressure: pressure_pin,
            },
            p.ADC1,
            &clocks,
            &mut rcc.apb2
        );

        // Flow sensor
        let mut flow_pin = gpioa.pa9.into_pull_up_input();
        flow_pin.make_interrupt_source(&mut p.SYSCFG, &mut rcc.apb2);
        flow_pin.enable_interrupt(&mut p.EXTI);
        flow_pin.trigger_on_edge(&mut p.EXTI, Edge::Falling);
        unsafe {
            NVIC::unmask(stm32f7xx_hal::interrupt::EXTI9_5);
        }

        let flow_pins = flow_sensor::Pins {
            flow: flow_pin.erase(),
        };
        let flow_sensor = flow_sensor::FlowSensor::new(flow_pins);

        // SPI peripherals
        let spi_pins = sense_bus::SpiPins {
            sclk: gpioi.pi1.into_alternate(),
            miso: gpioc.pc2.into_alternate(),
            mosi: gpioi.pi3.into_alternate(),
        };
        let mut spi_cs_pins = [
            gpioi.pi0.into_push_pull_output().erase(),
            gpioh.ph7.into_push_pull_output().erase(),
            gpioh.ph6.into_push_pull_output().erase(),
            gpiof.pf9.into_push_pull_output().erase(),
        ];
        let sense_sync_pin = gpioh.ph10.into_push_pull_output().erase();
        for pin in spi_cs_pins.iter_mut() {
            pin.set_high();
        }

        let sense_bus = sense_bus::SenseBus::new(
            spi_pins,
            spi_cs_pins,
            sense_sync_pin,
            p.SPI2,
            &clocks,
            &mut rcc.apb1
        );

        // Display
        let lcd_on = gpioe.pe4.into_push_pull_output();
        let lcd_bkl_pin = gpioe.pe5.into_alternate();
        let mut lcd_bkl = p.TIM9.pwm_hz(lcd_bkl_pin, 490.Hz(), &clocks).split();
        let _max = lcd_bkl.get_max_duty();
        lcd_bkl.set_duty(0);
        lcd_bkl.enable();

        let display = display::Display::new(
            lcd_on.erase(),
            p.LTDC,
            p.DMA2D
        );

        into_alternate_hs_pin!{
            14,
            gpiog.pg6,  // R7
            gpioj.pj5,  // R6
            gpioj.pj4,  // R5
            gpioj.pj3,  // R4
            gpioj.pj2,  // R3
            gpioj.pj1,  // R2

            gpiob.pb5,  // G7

            gpiok.pk6,  // B7
            gpiok.pk5,  // B6
            gpioi.pi5,  // B5
            gpiok.pk3,  // B4
            gpioj.pj15, // B3
            gpioj.pj14, // B2

            gpioi.pi14, // CLK
            gpioi.pi13, // VSYNC
            gpioi.pi12, // HSYNC
            gpiok.pk7   // DE
        }
        into_alternate_hs_pin!{
            9,
            gpioi.pi11, // G6
            gpioh.ph4,  // G5
            gpioj.pj13, // G4
            gpiog.pg10, // G3
            gpioi.pi15  // G2
        }

        // Memory bus components
        let fmc = fmc::Fmc {
            pins: fmc::Pins {
                a0: into_fmc_pin!(gpiof.pf0),
                a1: into_fmc_pin!(gpiof.pf1),
                a2: into_fmc_pin!(gpiof.pf2),
                a3: into_fmc_pin!(gpiof.pf3),
                a4: into_fmc_pin!(gpiof.pf4),
                a5: into_fmc_pin!(gpiof.pf5),
                a6: into_fmc_pin!(gpiof.pf12),
                a7: into_fmc_pin!(gpiof.pf13),
                a8: into_fmc_pin!(gpiof.pf14),
                a9: into_fmc_pin!(gpiof.pf15),
                a10: into_fmc_pin!(gpiog.pg0),
                a11: into_fmc_pin!(gpiog.pg1),
                a12: into_fmc_pin!(gpiog.pg2),

                d0: into_fmc_pin!(gpiod.pd14),
                d1: into_fmc_pin!(gpiod.pd15),
                d2: into_fmc_pin!(gpiod.pd0),
                d3: into_fmc_pin!(gpiod.pd1),
                d4: into_fmc_pin!(gpioe.pe7),
                d5: into_fmc_pin!(gpioe.pe8),
                d6: into_fmc_pin!(gpioe.pe9),
                d7: into_fmc_pin!(gpioe.pe10),
                d8: into_fmc_pin!(gpioe.pe11),
                d9: into_fmc_pin!(gpioe.pe12),
                d10: into_fmc_pin!(gpioe.pe13),
                d11: into_fmc_pin!(gpioe.pe14),
                d12: into_fmc_pin!(gpioe.pe15),
                d13: into_fmc_pin!(gpiod.pd8),
                d14: into_fmc_pin!(gpiod.pd9),
                d15: into_fmc_pin!(gpiod.pd10),

                ba0: into_fmc_pin!(gpiog.pg4),
                ba1: into_fmc_pin!(gpiog.pg5),

                nbl0: into_fmc_pin!(gpioe.pe0),
                nbl1: into_fmc_pin!(gpioe.pe1),
                sdncas: into_fmc_pin!(gpiog.pg15),
                sdclk: into_fmc_pin!(gpiog.pg8),
                sdnwe: into_fmc_pin!(gpioh.ph5),
                sdne0: into_fmc_pin!(gpioh.ph3),
                sdcke0: into_fmc_pin!(gpioh.ph2),
                sdnras: into_fmc_pin!(gpiof.pf11),
            },
            fmc: p.FMC,
        };
        fmc.sdram(&clocks, &mut delay);

        // NAND Flash

        // Ethernet
        #[cfg(feature = "ethernet")]
        let ethernet = {
            let eth_pins = ethernet::Pins {
                ref_clk: gpioa.pa1,
                crs: gpioa.pa7,
                tx_en: gpiog.pg11,
                tx_d0: gpiob.pb12,
                tx_d1: gpiob.pb13,
                rx_d0: gpioc.pc4,
                rx_d1: gpioc.pc5,
                mdio: gpioa.pa2.into_alternate::<11>(),
                mdclk: gpioc.pc1.into_alternate::<11>(),
                reset: gpioc.pc0.into_push_pull_output(),
                mco: gpioa.pa8.into_alternate::<0>(),
            };
            ethernet::Ethernet::new(
                eth_pins,
                p.ETHERNET_MAC,
                p.ETHERNET_DMA,
                clocks,
            )
        };

        #[cfg(feature = "sysclock")]
        let sys_clock = sysclock::SysClock::new(cp.SYST);

        //cp.SCB.enable_dcache(&mut cp.CPUID);
        cp.SCB.enable_icache();

        Board {
            buttons,
            state,
            actuators,
            analog_sensors,
            flow_sensor,
            sense_bus,
            display,
            #[cfg(feature = "ethernet")]
            ethernet,
            delay,
            #[cfg(feature = "sysclock")]
            sys_clock,
        }
    }
}


pub fn emergency_stop() {
    let p = unsafe { Peripherals::steal() };

    p.GPIOB.odr.write(|w| w
        .odr14().set_bit()
        .odr15().clear_bit()
    );
    p.GPIOH.odr.write(|w| w.odr8().clear_bit());
}


#[macro_export]
macro_rules! into_alternate_hs_pin {
    ($alt:literal, $($pin:expr),+) => {
        {
            $(
                $pin.into_alternate::<$alt>().set_speed(Speed::VeryHigh);
            )*
        }
    };
}