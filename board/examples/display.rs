#![no_std]
#![no_main]

use embedded_hal::prelude::_embedded_hal_blocking_delay_DelayMs;
use board::Board;
use cortex_m_rt::entry;
use stm32f7xx_hal::ltdc::{Layer, PixelFormat};
use panic_rtt_target as _;
use rtt_target::{rtt_init_print, rprintln};
use ui::UI;

mod img;
mod screen;

//#[link_section=".xram"]
//static mut FRAMEBUFFER: [u8; 1_536_000] = [0; 1_536_000];

const UI: UI = UI::new();

#[entry]
fn main() -> ! {
    let board = Board::new();
    rtt_init_print!();
    rprintln!("Bern Espresso - Display example");

    let view_model = ui::ViewModel {
        pressure_water: 8.7_f32,
        volume_water: 12.0_f32,
        temperature: ui::Temperature {
            boiler_inside: 97.1_f32,
            boiler_outside: 0.0_f32,
            group_head_inside: 94.4_f32,
            group_head_outside: 0.0_f32,
        }
    };

    let p = unsafe { stm32f7xx_hal::pac::Peripherals::steal() };

    let mut delay = board.delay;

    let mut display = screen::Stm32F7DiscoDisplay::new(
        p.LTDC,
        p.DMA2D,
    );

    /* display ferris from flash */
    // display.controller.config_layer(
    //     Layer::L1, unsafe { &mut *(&img::img_cal_argb8888::IMG_CAL as *const [u8; 1536000] as *mut [u8; 1536000]) },
    //     //Layer::L1, unsafe { &mut *(&img::img_ferris_argb8888::IMG_FERRIS as *const [u8; 1536000] as *mut [u8; 1536000]) },
    //     PixelFormat::ARGB8888
    // );

    display.controller.config_layer(
        //Layer::L1, unsafe { &mut FRAMEBUFFER },
        Layer::L1, unsafe { &mut (*(0xC0000000 as *mut [u8;1_536_000])) },
        PixelFormat::ARGB8888
    );

    display.controller.enable_layer(Layer::L1);
    display.controller.reload();

    UI.init();


    loop {
        UI.tick();
        delay.delay_ms(5u32);
        UI.update(&view_model);
    }
}