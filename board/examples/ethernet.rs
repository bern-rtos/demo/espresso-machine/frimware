#![no_std]
#![no_main]

use cortex_m::asm;
use stm32_eth::{smi, TxError};
use panic_rtt_target as _;
use rtt_target::{rtt_init_print, rprintln};

use smoltcp_nal::smoltcp::wire::{
    ArpOperation, ArpPacket, ArpRepr, EthernetAddress, EthernetFrame, EthernetProtocol,
    EthernetRepr, Ipv4Address,
};

use board::Board;


const PHY_REG_BSR: u8 = 0x01;
const PHY_REG_BSR_UP: u16 = 1 << 2;

const PHY_ADDR: u8 = 0;

#[cortex_m_rt::entry]
fn main() -> ! {
    let board = Board::new();
    rtt_init_print!();
    rprintln!("Bern Espresso - Ethernet example");

    let mut eth = board.ethernet.eth;
    let mut smi_pins = board.ethernet.smi_pins;

    let mut last_link_up = false;

    loop {
        let link_up = link_detected(eth.smi(&mut smi_pins.mdio, &mut smi_pins.mdclk));

        if link_up != last_link_up {
            if link_up {
                rprintln!("Ethernet: link detected");
            } else {
                rprintln!("Ethernet: no link detected");
            }
            last_link_up = link_up;
        }

        if link_up {
            const SIZE: usize = 14 + 28; // ETH + ARP

            let src_mac = EthernetAddress::from_bytes(&[0x00, 0x00, 0xDE, 0xAD, 0xBE, 0xEF]);

            let arp_buffer = [0; 28];
            let mut packet =
                ArpPacket::new_checked(arp_buffer).expect("ArpPacket: buffer size is not correct");
            let arp = ArpRepr::EthernetIpv4 {
                operation: ArpOperation::Request,
                source_hardware_addr: src_mac,
                source_protocol_addr: Ipv4Address::new(192, 168, 1, 100),
                target_hardware_addr: EthernetAddress::from_bytes(&[0x00; 6]),
                target_protocol_addr: Ipv4Address::new(10, 0, 0, 2),
            };
            arp.emit(&mut packet);

            let eth_buffer = [0; SIZE]; // ETH + ARP
            let mut frame = EthernetFrame::new_checked(eth_buffer)
                .expect("EthernetFrame: buffer size is not correct");
            let header = EthernetRepr {
                src_addr: src_mac,
                dst_addr: EthernetAddress::BROADCAST,
                ethertype: EthernetProtocol::Arp,
            };
            header.emit(&mut frame);
            frame.payload_mut().copy_from_slice(&packet.into_inner());

            let r = eth.send(SIZE, |buf| {
                buf[0..SIZE].copy_from_slice(&frame.into_inner());
            });

            match r {
                Ok(()) => {
                    rprintln!("ARP-smoltcp sent");
                }
                Err(TxError::WouldBlock) => rprintln!("ARP failed"),
            }
        } else {
            rprintln!("Down");
        }

        cortex_m::interrupt::free(|cs| {
            let mut eth_pending = ETH_PENDING.borrow(cs).borrow_mut();
            *eth_pending = false;

            if !*eth_pending {
                asm::wfi();
            }
        });
    }
}



use core::cell::RefCell;
static ETH_PENDING: cortex_m::interrupt::Mutex<RefCell<bool>> = cortex_m::interrupt::Mutex::new(RefCell::new(false));
use stm32f7xx_hal::pac::{interrupt, Peripherals};

#[allow(non_snake_case)]
#[interrupt]
fn ETH() {
    cortex_m::interrupt::free(|cs| {
        let mut eth_pending = ETH_PENDING.borrow(cs).borrow_mut();
        *eth_pending = true;
    });

    // Clear interrupt flags
    let p = unsafe { Peripherals::steal() };
    stm32_eth::eth_interrupt_handler(&p.ETHERNET_DMA);
}


fn link_detected<Mdio, Mdc>(smi: smi::Smi<Mdio, Mdc>) -> bool
    where
        Mdio: smi::MdioPin,
        Mdc: smi::MdcPin,
{
    let status = smi.read(PHY_ADDR, PHY_REG_BSR);
    (status & PHY_REG_BSR_UP) == PHY_REG_BSR_UP
}