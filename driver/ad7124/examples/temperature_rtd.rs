#![no_std]
#![no_main]

use board::Board;
use cortex_m_rt::entry;
use embedded_hal::blocking::delay::DelayMs;
use panic_rtt_target as _;
use rtt_target::{rtt_init_print, rprintln};
use nb::block;
use ad7124::Error;
use ad7124::preset::CALLENDAR_VAN_DUSEN_IEC60751;
use ad7124::preset::rtd_3wire::{Ad7124Rtd3W, RTDChannel, Config, Circuit, RTD3WIO};
use ad7124::register::{ExcitationCurrent, Gain, Input};


const CIRCUIT: Circuit<4> = Circuit {
    rtd_ios: [
        RTD3WIO {
            sense_p: Input::AIN1,
            sense_n: Input::AIN2,
            source_p: 0,
            source_n: 3,
        },
        RTD3WIO {
            sense_p: Input::AIN5,
            sense_n: Input::AIN6,
            source_p: 4,
            source_n: 7,
        },
        RTD3WIO {
            sense_p: Input::AIN9,
            sense_n: Input::AIN10,
            source_p: 8,
            source_n: 11,
        },
        RTD3WIO {
            sense_p: Input::AIN13,
            sense_n: Input::AIN14,
            source_p: 12,
            source_n: 15,
        },
    ],
    ref_resistor: 5110,
};

#[entry]
fn main() -> ! {
    let board = Board::new();
    rtt_init_print!();

    let sense_bus = board.sense_bus;
    let adc = ad7124::Ad7124::new(sense_bus).unwrap();

    // e.g. read from EEPROM
    let channel_config = Config {
        rtd_channels:  [
            Some(RTDChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
            Some(RTDChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
            Some(RTDChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
            Some(RTDChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
        ],
        current: ExcitationCurrent::_250uA,
        gain: Gain::_32,
    };

    let mut sense_rtd = Ad7124Rtd3W::new(adc, channel_config, CIRCUIT).unwrap();
    sense_rtd.calibrate().unwrap();

    let mut delay = board.delay;
    loop {
        for ch in 0..4 {
            sense_rtd.set_active_channel(ch).unwrap();
            // Wait to discard some measurements until the filter has settled.
            delay.delay_ms(100u32);

            match block!(sense_rtd.read_temperature()) {
                Ok(t) => rprintln!("Temperature ({}): {:.02}", ch, t),
                Err(Error::InvalidMeasurement) => rprintln!("Temperature ({}): NC", ch),
                Err(_) => {}
            }

            if ch == 3 {
                rprintln!("-----------------------------");
            }
        }
    }
}