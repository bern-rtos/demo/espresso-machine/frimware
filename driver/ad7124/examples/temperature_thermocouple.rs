#![no_std]
#![no_main]

use board::Board;
use cortex_m_rt::entry;
use embedded_hal::blocking::delay::DelayMs;
use panic_rtt_target as _;
use rtt_target::{rtt_init_print, rprintln};
use nb::block;
use ad7124::{Ad7124, Error};
use ad7124::preset::CALLENDAR_VAN_DUSEN_IEC60751;
use ad7124::preset::thermocouple::{Ad7124Thermocouple, ThermocoupleIO, ColdJunctionIO, Circuit, Config, ThermocoupleChannel, ColdJunctionChannel};
use ad7124::register::{ExcitationCurrent, Gain, Input};


const CIRCUIT: Circuit<4> = Circuit {
    thermocouple_ios: [
        ThermocoupleIO {
            sense_p: Input::AIN0,
            sense_n: Input::AIN1,
        },
        ThermocoupleIO {
            sense_p: Input::AIN2,
            sense_n: Input::AIN3,
        },
        ThermocoupleIO {
            sense_p: Input::AIN4,
            sense_n: Input::AIN5,
        },
        ThermocoupleIO {
            sense_p: Input::AIN6,
            sense_n: Input::AIN7,
        },
    ],
    cold_junction: ColdJunctionIO {
        sense_p: Input::AIN14,
        sense_n: Input::AIN15,
        source_p: 13,
        ref_resistor: 5110,
    }
};

#[entry]
fn main() -> ! {
    let board = Board::new();
    rtt_init_print!();
    rprintln!("AD7124-8 Thermocuple Example");

    let sense_bus = board.sense_bus;
    let adc = Ad7124::new(sense_bus).unwrap();

    // e.g. read from EEPROM
    let channel_config = Config {
        thermocouple_channels: [
            Some(ThermocoupleChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
            Some(ThermocoupleChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
            Some(ThermocoupleChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
            Some(ThermocoupleChannel {
                coeff: CALLENDAR_VAN_DUSEN_IEC60751,
            }),
        ],
        cold_junction_channel: ColdJunctionChannel {
            current: ExcitationCurrent::_500uA,
            gain: Gain::_32,
            coeff: CALLENDAR_VAN_DUSEN_IEC60751,
        },
        gain: Gain::_128,
    };

    let mut sense_thermocouple = Ad7124Thermocouple::new(adc, channel_config, CIRCUIT).unwrap();
    //sense_thermocouple.calibrate().unwrap();

    let mut delay = board.delay;
    loop {

        //sense_thermocouple.set_active_channel(ch).unwrap();
        // Wait to discard some measurements until the filter has settled.
        delay.delay_ms(100u32);

        /*match block!(sense_thermocouple.read_cold_junction()) {
            Ok(t) => rprintln!("Temperature (cold junction): {:.02}", t),
            Err(Error::InvalidMeasurement) => rprintln!("Temperature (cold junction): NC"),
            Err(_) => {}
        };*/

        match block!(sense_thermocouple.read_thermocouple()) {
            Ok(t) => rprintln!("Temperature (0): {:.04}", t),
            Err(Error::InvalidMeasurement) => rprintln!("Temperature (cold junction): NC"),
            Err(_) => {}
        };

        //if ch == 3 {
        //    rprintln!("-----------------------------");
        //}
    }
}