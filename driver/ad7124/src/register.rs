//! # AD7124-8 Register
//!
//! Rust representation of the Analog Devices AD7124-8 8-Channel, Low Noise,
//! Low Power, 24-Bit, Sigma-Delta ADC with PGA and Reference.
//!
//! <https://www.analog.com/en/products/ad7124-8.html>

use bern_register_generator::{register, register_enum};
pub use bern_register_generator::traits::*;

register!{
    /// Communications Register
    ///
    /// The communications register is an 8-bit, write only register. All
    /// communications to the device must start with a write operation
    /// to the communications register. The data written to the communi-
    /// cations register determines whether the next operation is a read
    /// or write operation, and to which register this operation takes
    /// place, the RS[5:0] bits selecting the register to be accessed.
    ///
    /// For read or write operations, after the subsequent read or write
    /// operation to the selected register is complete, the interface
    /// returns to where it expects a write operation to the
    /// communications register. This is the default state of the interface
    /// and, on power-up or after a reset, the ADC is in this default
    /// state waiting for a write operation to the communications
    /// register.
    ///
    /// In situations where the interface sequence is lost, a write
    /// operation of at least 64 serial clock cycles with DIN high returns
    /// the ADC to this default state by resetting the entire device.
    /// Table 65 outlines the bit designations for the communications
    /// register. Bit 7 denotes the first bit of the data stream.
    COMMS(0x00 | rw): B8 {
        /// Write enable bit. A 0 must be written to this bit so that the write to the communications register actually occurs. If
        /// a 1 is the first bit written, the device does not clock on to subsequent bits in the register. It stays at this bit location
        /// until a 0 is written to this bit. As soon as a 0 is written to the WEN bit, the next seven bits are loaded to the
        /// communications register.
        WEN: [7],
        /// A 0 in this bit location indicates that the next operation is a write to a specified register. A 1 in this position
        /// indicates that the next operation is a read from the designated register.
        RW: [6],
        /// Register address bits. These address bits select which registers of the ADC are being selected during this serial
        /// interface communication.
        RS: [5..0],
    }
}

register!{
    /// Status Register
    ///
    /// The status register is an 8-bit, read only register. To access the ADC status register, the user must write to the communications register,
    /// select the next operation to be read, and set the register address bits RS[5:0] to 0.
    STATUS(0x00 | r): B8 {
        /// Ready bit for the ADC. This bit is cleared when data is written to the ADC data register. The RDY bit is set
        /// automatically after the ADC data register is read or a period of time before the data register is updated with a
        /// new conversion result to indicate to the user not to read the conversion data. It is also set when the device is
        /// placed in power-down or standby mode. The end of a conversion is also indicated by the DOUT/RDY pin. This
        /// pin can be used as an alternative to the status register for monitoring the ADC for conversion data.
        RDY: [7],
        /// ADC error bit. This bit indicates if one of the error bits has been asserted in the error register. This bit is high if
        /// one or more of the error bits in the error register has been set. This bit is cleared by a read of the error register.
        ERROR_FLAG: [6],
        /// Power-on reset flag. This bit indicates when a power-on reset occurs. A power-on reset occurs on power-up,
        /// when the power supply voltage goes below a threshold voltage, when a reset is performed, and when coming
        /// out of power-down mode. The status register must be read to clear the bit.
        POR_FLAG: [4],
        /// These bits indicate which channel is being converted by the ADC.
        CH_ACTIVE: [3..0],
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    pub enum PowerMode {
        LowPower = 0b00,
        MidPower = 0b01,
        FullPower = 0b10,
        __FullPower2 = 0b11,
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    pub enum OperationMode {
        /// Continuous conversion mode (default). In continuous conversion mode, the ADC continuously performs conversions and places
        /// the result in the data register. RDY goes low when a conversion is complete. The user can read these conversions by placing the device
        /// in continuous read mode whereby the conversions are automatically placed on the DOUT line when SCLK pulses are applied.
        /// Alternatively, the user can instruct the ADC to output the conversion by writing to the communications register. After power-on,
        /// a reset, or a reconfiguration of the ADC, the complete settling time of the filter is required to generate the first valid conversion.
        /// Subsequent conversions are available at the selected output data rate, which is dependent on filter choice.
        ContinuousConversion = 0b0000,
        /// Single conversion mode. When single conversion mode is selected, the ADC powers up and performs a single conversion on the
        /// selected channel. The conversion requires the complete settling time of the filter. The conversion result is placed in the data
        /// register, RDY goes low, and the ADC returns to standby mode. The conversion remains in the data register and RDY remains
        /// active (low) until the data is read or another conversion is performed.
        SingleConversion = 0b0001,
        /// Standby mode. In standby mode, all sections of the AD7124-8 can be powered down except the LDOs. The internal reference,
        /// on-chip oscillator, low-side power switch, and bias voltage generator can be enabled or disabled while in standby mode. The
        /// on-chip registers retain their contents in standby mode.
        /// Any enabled diagnostics remain active when the ADC is in standby mode. The diagnostics can be enabled/disabled while in
        /// standby mode. However, any diagnostics that require the master clock (reference detect, undervoltage/overvoltage detection,
        /// LDO trip tests, memory map CRC, and MCLK counter) must be enabled when the ADC is in continuous conversion mode or idle
        /// mode; these diagnostics do not function if enabled in standby mode.
        Standby = 0b0010,
        /// Power-down mode. In power-down mode, all the AD7124-8 circuitry is powered down, including the current sources, power
        /// switch, burnout currents, bias voltage generator, and clock circuitry. The LDOs are also powered down. In power-down mode, the
        /// on-chip registers do not retain their contents. Therefore, coming out of power-down mode, all registers must be reprogrammed.
        PowerDown = 0b0011,
        /// Idle mode. In idle mode, the ADC filter and modulator are held in a reset state even though the modulator clocks continue to be
        /// provided.
        Idle = 0b0100,
        /// Internal zero-scale (offset) calibration. An internal short is automatically connected to the input. RDY goes high when the
        /// calibration is initiated and returns low when the calibration is complete. The ADC is placed in idle mode following a calibration.
        /// The measured offset coefficient is placed in the offset register of the selected channel. Select only one channel when zero-scale
        /// calibration is being performed. An internal zero-scale calibration takes a time of one settling period to be performed.
        CalIntZeroScale = 0b0101,
        /// Internal full-scale (gain) calibration. A full-scale input voltage is automatically connected to the selected analog input for this
        /// calibration. RDY goes high when the calibration is initiated and returns low when the calibration is complete. The ADC is placed
        /// in idle mode following a calibration. The measured full-scale coefficient is placed in the gain register of the selected channel. A
        /// full-scale calibration is required each time the gain of a channel is changed to minimize the full-scale error. Select only one
        /// channel when full-scale calibration is being performed. The AD7124-8 is factory calibrated at a gain of 1. Further internal
        /// full-scale calibrations at a gain of 1 are not supported. An internal full-scale calibration (gain > 1) takes a time of four settling
        /// periods.
        /// Internal full-scale calibrations cannot be performed in the full power mode. So, if using the full-power mode, select mid or low
        /// power mode for the internal full-scale calibration. This calibration is valid in full power mode as the same reference and gain are
        /// used. When performing internal zero-scale and internal full-scale calibrations, the internal full-scale calibration must be
        /// performed before the internal zero-scale calibration. Therefore, write 0x800000 to the offset register before performing any
        /// internal full-scale calibration, which resets the offset register to its default value.
        CalIntFullScale = 0b0110,
        /// System zero-scale (offset) calibration. Connect the system zero-scale input to the channel input pins of the selected channel. RDY
        /// goes high when the calibration is initiated and returns low when the calibration is complete. The ADC is placed in idle mode
        /// following a calibration. The measured offset coefficient is placed in the offset register of the selected channel. A system zero-
        /// scale calibration is required each time the gain of a channel is changed. Select only one channel when full-scale calibration is
        /// being performed. A system zero-scale calibration takes a time of one settling period to be performed.
        CalSysZeroScale = 0b0111,
        /// System full-scale (gain) calibration. Connect the system full-scale input to the channel input pins of the selected channel. RDY goes
        /// high when the calibration is initiated and returns low when the calibration is complete. The ADC is placed in idle mode following
        /// a calibration. The measured full-scale coefficient is placed in the gain register of the selected channel. A full-scale calibration is
        /// required each time the gain of a channel is changed. Select only one channel when full-scale calibration is being performed. A
        /// system full-scale calibration takes a time of one settling period to be performed.
        CalSysFullScale = 0b1000,
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    #[allow(non_camel_case_types)]
    pub enum Clock {
        /// Internal 614.4 kHz clock. The internal clock is not available at the CLK pin.
        Internal_614kHz4_NoOut = 0b00,
        /// Internal 614.4 kHz clock. This clock is available at the CLK pin.
        Internal_614kHz4_Out = 0b01,
        /// External 614.4 kHz clock.
        External_614kHz4 = 0b10,
        /// External clock. The external clock is divided by 4 within the AD7124-8.
        External = 0b11
    }
}

register!{
    /// ADC Control Register
    ADC_CONTROL(0x01 | rw): B16 {
        /// Controls the SCLK inactive edge to DOUT/RDY high time. When DOUT_RDY_DEL is cleared, the delay is
        /// 10 ns minimum. When DOUT_RDY_DEL is set, the delay is increased to 100 ns minimum. This function is
        /// useful when CS is tied low (the CS_EN bit is set to 0).
        DOUT_RDY_DEL: [12],
        ///Continuous read of the data register. When this bit is set to 1 (and the data register is selected), the serial
        /// interface is configured so that the data register can be continuously read; that is, the contents of the data
        /// register are automatically placed on the DOUT pin when the SCLK pulses are applied after the RDY pin
        /// goes low to indicate that a conversion is complete. The communications register does not have to be
        /// written to for subsequent data reads. To enable continuous read, the CONT_READ bit is set. To disable
        /// continuous read, write a read data command while the DOUT/ RDY pin is low. While continuous read is
        /// enabled, the ADC monitors activity on the DIN line so that it can receive the instruction to disable
        /// continuous read. Additionally, a reset occurs if 64 consecutive 1s occur on DIN; therefore, hold DIN low
        /// until an instruction is written to the device.
        CONT_READ: [11],
        /// This bit enables the transmission of the status register contents after each data register read. When
        /// DATA_STATUS is set, the contents of the status register are transmitted along with each data register
        /// read. This function is useful when several channels are selected because the status register identifies the
        /// channel to which the data register value corresponds.
        DATA_STATUS: [10],
        /// This bit controls the operation of the DOUT/RDY pin during read operations.
        /// When CS_EN is cleared, the DOUT pin returns to being a RDY pin within nanoseconds of the SCLK
        /// inactive edge (the delay is determined by the DOUT_RDY_DEL bit).
        /// When set, the DOUT/RDY pin continues to output the LSB of the register being read until CS is taken
        /// high. CS must frame all read operations when CS_EN is set. CS_EN must be set to use the diagnostic
        /// functions SPI_WRITE_ERR, SPI_READ_ERR, and SPI_SCLK_CNT_ERR.
        CS_EN: [9],
        /// Internal reference voltage enable. When this bit is set, the internal reference is enabled and available at
        /// the REFOUT pin. When this bit is cleared, the internal reference is disabled.
        REF_EN: [8],
        /// Power Mode Select. These bits select the power mode. The current consumption and output data rate
        /// ranges are dependent on the power mode.
        POWER_MODE(PowerMode): [7..6],
        /// These bits control the mode of operation for ADC.
        MODE(OperationMode): [5..2],
        /// These bits select the clock source for the ADC. Either the on-chip 614.4 kHz clock can be used or an
        /// external clock can be used. The ability to use an external clock allows several AD7124-8 devices to be
        /// synchronized. Also, 50 Hz and 60 Hz rejection is improved when an accurate external clock drives the ADC.
        CLK_SEL(Clock): [1..0],
    }
}

// todo: 24 bit register -> only lowest 8 bit implemented
register!{
    /// Data Register
    ///
    /// The conversion result from the ADC is stored in this data register. This is a read-only register. On completion of a read operation from
    /// this register, the RDY bit/pin is set.
    DATA(0x02 | r): B24 {
        VALUE: [23..0],
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    #[allow(non_camel_case_types)]
    pub enum ExcitationCurrent {
        _0uA = 0b000,
        _50uA = 0b001,
        _100uA = 0b010,
        _250uA = 0b011,
        _500uA = 0b100,
        _750uA = 0b101,
        _1000uA = 0b110,
        __1000uA_2 = 0b111,
    }
}

register!{
    /// IO Control Register 1
    IO_CONTROL_1(0x03 | rw): B24 {
        /// Digital Output P4. When GPIO_CTRL4 is set, the GPIO_DAT4 bit sets the value of the P4 general-purpose
        /// output pin. When GPIO_DAT4 is high, the P4 output pin is high. When GPIO_DAT4 is low, the P4 output
        /// pin is low. When the IO_CONTROL_1 register is read, the GPIO_DAT4 bit reflects the status of the P4 pin if
        /// GPIO_CTRL4 is set.
        GPIO_DAT4: [23],
        /// Digital Output P3. When GPIO_CTRL3 is set, the GPIO_DAT3 bit sets the value of the P3 general-purpose
        /// output pin. When GPIO_DAT3 is high, the P3 output pin is high. When GPIO_DAT3 is low, the P3 output
        /// pin is low. When the IO_CONTROL_1 register is read, the GPIO_DAT3 bit reflects the status of the P3 pin if
        /// GPIO_CTRL3 is set.
        GPIO_DAT3: [22],
        /// Digital Output P2. When GPIO_CTRL2 is set, the GPIO_DAT2 bit sets the value of the P2 general-purpose
        /// output pin. When GPIO_DAT2 is high, the P2 output pin is high. When GPIO_DAT2 is low, the P2 output
        /// pin is low. When the IO_CONTROL_1 register is read, the GPIO_DAT2 bit reflects the status of the P2 pin if
        /// GPIO_CTRL2 is set.
        GPIO_DAT2: [21],
        /// Digital Output P1. When GPIO_CTRL1 is set, the GPIO_DAT1 bit sets the value of the P1 general-purpose
        /// output pin. When GPIO_DAT1 is high, the P1 output pin is high. When GPIO_DAT1 is low, the P1 output
        /// pin is low. When the IO_CONTROL_1 register is read, the GPIO_DAT1 bit reflects the status of the P1 pin if
        /// GPIO_CTRL1 is set.
        GPIO_DAT1: [20],
        /// Digital Output P4 enable. When GPIO_CTRL4 is set, the digital output P4 is active. When GPIO_CTRL4 is
        /// cleared, the pin functions as analog input pin AIN5.
        GPIO_CTRL4: [19],
        /// Digital Output P3 enable. When GPIO_CTRL3 is set, the digital output P3 is active. When GPIO_CTRL3 is
        /// cleared, the pin functions as analog input pin AIN4.
        GPIO_CTRL3: [18],
        /// Digital Output P2 enable. When GPIO_CTRL2 is set, the digital output P2 is active. When GPIO_CTRL2 is
        /// cleared, the pin functions as analog input pin AIN3.
        GPIO_CTRL2: [17],
        /// Digital Output P1 enable. When GPIO_CTRL1 is set, the digital output P1 is active. When GPIO_CTRL1 is
        /// cleared, the pin functions as analog input pin AIN2.
        GPIO_CTRL1: [16],
        /// Bridge power-down switch control bit. Set this bit to close the bridge power-down switch PDSW to
        /// AGND. The switch can sink up to 30 mA. Clear this bit to open the bridge power-down switch. When the
        /// ADC is placed in standby mode, the bridge power-down switch remains active.
        PDSW: [15],
        /// These bits set the value of the excitation current for IOUT1.
        IOUT1(ExcitationCurrent): [13..11],
        /// These bits set the value of the excitation current for IOUT2.
        IOUT0(ExcitationCurrent): [10..8],
        /// Channel select bits for the excitation current for IOUT1 (AIN<N>).
        IOUT1_CH: [7..4],
        /// Channel select bits for the excitation current for IOUT0 (AIN<N>).
        IOUT0_CH: [3..0],
    }
}

register!{
    /// IO Control Register 2
    IO_CONTROL_2(0x04 | rw): B16 {
        /// Enable the bias voltage on the AIN15 channel. When set, the internal bias voltage is available on AIN15.
        VBIAS15: [15],
        /// Enable the bias voltage on the AIN14 channel. When set, the internal bias voltage is available on AIN14.
        VBIAS14: [14],
        /// Enable the bias voltage on the AIN13 channel. When set, the internal bias voltage is available on AIN13.
        VBIAS13: [13],
        /// Enable the bias voltage on the AIN12 channel. When set, the internal bias voltage is available on AIN12.
        VBIAS12: [12],
        /// Enable the bias voltage on the AIN11 channel. When set, the internal bias voltage is available on AIN11.
        VBIAS11: [11],
        /// Enable the bias voltage on the AIN10 channel. When set, the internal bias voltage is available on AIN10.
        VBIAS10: [10],
        /// Enable the bias voltage on the AIN9 channel. When set, the internal bias voltage is available on AIN9.
        VBIAS9: [9],
        /// Enable the bias voltage on the AIN8 channel. When set, the internal bias voltage is available on AIN8.
        VBIAS8: [8],
        /// Enable the bias voltage on the AIN7 channel. When set, the internal bias voltage is available on AIN7.
        VBIAS7: [7],
        /// Enable the bias voltage on the AIN6 channel. When set, the internal bias voltage is available on AIN6.
        VBIAS6: [6],
        /// Enable the bias voltage on the AIN5 channel. When set, the internal bias voltage is available on AIN5.
        VBIAS5: [5],
        /// Enable the bias voltage on the AIN4 channel. When set, the internal bias voltage is available on AIN4.
        VBIAS4: [4],
        /// Enable the bias voltage on the AIN3 channel. When set, the internal bias voltage is available on AIN3.
        VBIAS3: [3],
        /// Enable the bias voltage on the AIN2 channel. When set, the internal bias voltage is available on AIN2.
        VBIAS2: [2],
        /// Enable the bias voltage on the AIN1 channel. When set, the internal bias voltage is available on AIN1.
        VBIAS1: [1],
        /// Enable the bias voltage on the AIN0 channel. When set, the internal bias voltage is available on AIN0.
        VBIAS0: [0],
    }
}


register!{
    /// Identification Register
    ID(0x05 | r): B8 {
        DEVICE_ID: [7..4],
        SILICON_REVISION: [3..0],
    }
}


register!{
    /// Error Register
    ///
    /// Diagnostics, such as checking overvoltages and checking the SPI interface, are included on the AD7124-8. The error register contains the
    /// flags for the different diagnostic functions. The functions are enabled and disabled using the ERROR_EN register.
    ERROR(0x06 | r): B24 {
        /// Analog/digital LDO decoupling capacitor check. This flag is set if the decoupling capacitors required for the
        /// analog and digital LDOs are not connected to the AD7124-8.
        LDO_CAP_ERR: [19],
        /// Calibration check. If a calibration is initiated but not completed, this flag is set to indicate that an error
        /// occurred during the calibration. The associated calibration register is not updated.
        ADC_CAL_ERR: [18],
        /// This bit indicates whether a conversion is valid. This flag is set if an error occurs during a conversion.
        ADC_CONV_ERR: [17],
        /// ADC saturation flag. This flag is set if the modulator is saturated during a conversion.
        ADC_SAT_ERR: [16],
        /// Overvoltage detection on AINP.
        AINP_OV_ERR: [15],
        /// Undervoltage detection on AINP.
        AINP_UV_ERR: [14],
        /// Overvoltage detection on AINM.
        AINM_OV_ERR: [13],
        /// Undervoltage detection on AINM.
        AINM_UV_ERR: [12],
        /// Reference detection. This flag indicates when the external reference being used by the ADC is open circuit or
        /// less than 0.7 V.
        REF_DET_ERR: [10],
        /// Digital LDO error. This flag is set if an error is detected with the digital LDO.
        DLDO_PSM_ERR: [9],
        /// Analog LDO error. This flag is set if an error is detected with the analog LDO voltage.
        ALDO_PSM_ERR: [7],
        /// When a CRC check of the internal registers is being performed, the on-chip registers cannot be written to.
        /// User write instructions are ignored by the ADC. This bit is set when the CRC check of the registers is
        /// occurring. The bit is cleared when the check is complete; write operations can only be performed then.
        SPI_IGNORE_ERR: [6],
        /// All serial communications are some multiple of eight bits. This bit is set when the number of SCLK cycles is
        /// not a multiple of eight.
        SPI_SCLK_CNT_ERR: [5],
        /// This bit is set when an error occurs during an SPI read operation.
        SPI_READ_ERR: [4],
        /// This bit is set when an error occurs during an SPI write operation.
        SPI_WRITE_ERR: [3],
        /// This bit is set if an error occurs in the CRC check of the serial communications.
        SPI_CRC_ERR: [2],
        /// Memory map error. A CRC calculation is performed on the memory map each time that the registers are
        /// written to. Following this, periodic CRC checks are performed on the on-chip registers. If the register
        /// contents have changed, the MM_CRC_ERR bit is set.
        MM_CRC_ERR: [1],
        /// ROM error. A CRC calculation is performed on the ROM contents (contains the default register values) on
        /// power-up. If the contents have changed, the ROM_CRC_ERR bit is set.
        ROM_CRC_ERR: [0],
    }
}


register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    pub enum CheckLdoCap {
        /// Check is not enabled.
        NoCheck = 0b00,
        /// Check the analog LDO capacitor.
        CheckAnalogCap = 0b01,
        /// Check the digital LDO capacitor.
        CheckDigitalCap = 0b10,
        /// Check is not enabled.
        __NoCheck2 = 0b11,
    }
}


register!{
    /// Error Enable Register
    ///
    /// All the diagnostic functions can be enabled or disabled by setting the appropriate bits in this register.
    ERROR_EN(0x07 | rw): B24 {
        /// Master clock counter. When this bit is set, the master clock counter is enabled and the result is
        /// reported via the MCLK_COUNT register. The counter monitors the master clock being used by the
        /// ADC. If an external clock is the clock source, the MCLK counter monitors this external clock.
        /// Similarly, if the on-chip oscillator is selected as the clock source to the ADC, the MCLK counter
        /// monitors the on-chip oscillator.
        MCLK_CNT_EN: [22],
        /// Test of analog/digital LDO decoupling capacitor check. When this bit is set, the decoupling
        /// capacitor is internally disconnected from the LDO, forcing a fault condition. This allows the user to
        /// test the circuitry that is used for the analog and digital LDO decoupling capacitor check.
        LDO_CAP_CHK_TEST_EN: [21],
        /// Analog/digital LDO decoupling capacitor check. These bits enable the capacitor check. When a
        /// check is enabled, the ADC checks for the presence of the external decoupling capacitor on
        LDO_CAP_CHK(CheckLdoCap): [20..19],
        /// When this bit is set, the calibration fail check is enabled.
        ADC_CAL_ERR_EN: [18],
        /// When this bit is set, the conversions are monitored and the ADC_CONV_ERR bit is set when a
        /// conversion fails.
        ADC_CONV_ERR_EN: [17],
        /// When this bit is set, the ADC modulator saturation check is enabled.
        ADC_SAT_ERR_EN: [16],
        /// When this bit is set, the overvoltage monitor on all enabled AINP channels is enabled.
        AINP_OV_ERR_EN: [15],
        /// When this bit is set, the undervoltage monitor on all enabled AINP channels is enabled.
        AINP_UV_ERR_EN: [14],
        /// When this bit is set, the overvoltage monitor on all enabled AINM channels is enabled.
        AINM_OV_ERR_EN: [13],
        /// When this bit is set, the undervoltage monitor on all enabled AINM channels is enabled.
        AINM_UV_ERR_EN: [12],
        /// When this bit is set, any external reference being used by the ADC is continuously monitored. An
        /// error is flagged if the external reference is open circuit or has a value of less than 0.7 V.
        REF_DET_ERR_EN: [11],
        /// Checks the test mechanism that monitors the digital LDO. When this bit is set, the input to the test
        /// circuit is tied to DGND instead of the LDO output. Set the DLDO_PSM_ERR bit in the error register.
        DLDO_PSM_TRIP_TEST_EN: [10],
        /// When this bit is set, the digital LDO voltage is continuously monitored. The DLDO_PSM_ERR bit in
        /// the error register is set if the voltage being output from the digital LDO is outside specification.
        DLDO_PSM_ERR_ERR: [9],
        /// Checks the test mechanism that monitors the analog LDO. When this bit is set, the input to the
        /// test circuit is tied to AVSS instead of the LDO output. Set the ALDO_PSM_ERR bit in the error
        /// register.
        ALDO_PSM_TRIP_TEST_EN: [8],
        /// When this bit is set, the analog LDO voltage is continuously monitored. The ALDO_PSM_ERR bit in
        /// the error register is set if the voltage being output from the analog LDO is outside specification.
        ALDO_PSM_ERR_EN: [7],
        /// When a CRC check of the internal registers is being performed, the on-chip registers cannot be
        /// written to. User write instructions are ignored by the ADC. Set this bit so that the SPI_IGNORE_ERR
        /// bit in the error register informs the user when write operations must not be performed.
        SPI_IGNORE_ERR_EN: [6],
        /// When this bit is set, the SCLK counter is enabled. All read and write operations to the ADC are
        /// multiples of eight bits. For every serial communication, the SCLK counter counts the number of
        /// SCLK pulses. CS must be used to frame each read and write operation. If the number of SCLK
        /// pulses used during a communication is not a multiple of eight, the SPI_SCLK_CNT_ERR bit in the
        /// error register is set. For example, a glitch on the SCLK pin during a read or write operation can be
        /// interpreted as an SCLK pulse. In this case, the SPI_SCLK_CNT_ERR bit is set as there is an excessive
        /// number of SCLK pulses detected. CS_EN in the ADC_CONTROL register must be set to 1 when the
        /// SCLK counter function is being used.
        SPI_SCLK_CNT_ERR_EN: [5],
        /// When this bit is set, the SPI_READ_ERR bit in the error register is set when an error occurs during a
        /// read operation. An error occurs if the user attempts to read from invalid addresses.
        /// CS_EN in the ADC_CONTROL register must be set to 1 when the SPI read check function is being
        /// used.
        SPI_READ_ERR_EN: [4],
        /// When this bit is set, the SPI_WRITE_ERR bit in the error register is set when an error occurs during a
        /// write operation. An error occurs if the user attempts to write to invalid addresses or write to read-
        /// only registers. CS_EN in the ADC_CONTROL register must be set to 1 when the SPI write check
        /// function is being used.
        SPI_WRITE_ERR_EN: [3],
        /// This bit enables a CRC check of all read and write operations. The SPI_CRC_ERR bit in the error
        /// register is set if the CRC check fails. In addition, an 8-bit CRC word is appended to all data read
        /// from the AD7124-8.
        SPI_CRC_ERR_EN: [2],
        /// When this bit is set, a CRC calculation is performed on the memory map each time that the
        /// registers are written to. Following this, periodic CRC checks are performed on the on-chip
        /// registers. If the register contents have changed, the MM_CRC_ERR bit is set.
        MM_CRC_ERR_EN: [1],
        /// When this bit is set, a CRC calculation is performed on the ROM contents on power-on. If the ROM
        /// contents have changed, the ROM_CRC_ERR bit is set.
        ROM_CRC_ERR_EN: [0],
    }
}

register!{
    /// MCLK Count Regiser
    ///
    /// The master clock frequency can be monitored using this register.
    MCLK_COUNT(0x08 | r): B8 {
        /// This register allows the user to determine the frequency of the internal/external oscillator. Internally, a clock
        /// counter increments every 131 pulses of the sampling clock (614.4 kHz in full power mode, 153.6 kHz in mid power
        /// mode, and 768 kHz in low power mode). The 8-bit counter wraps around on reaching its maximum value. The
        /// counter output is read back via this register.
        /// Note that the incrementation of the register is asynchronous to the register read. If a register read coincides with
        /// the register incrementation, it is possible to read an invalid value. To prevent this, read the register four times rather
        /// than once, then read the register four times again at a later point. By reading four values, it is possible to identify the
        /// correct register value at the start and at the end of the timing instants.
        VALUE: [7..0],
    }
}


register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    #[allow(non_camel_case_types)]
    pub enum Input {
        AIN0 = 0b00000,
        AIN1 = 0b00001,
        AIN2 = 0b00010,
        AIN3 = 0b00011,
        AIN4 = 0b00100,
        AIN5 = 0b00101,
        AIN6 = 0b00110,
        AIN7 = 0b00111,
        AIN8 = 0b01000,
        AIN9 = 0b01001,
        AIN10 = 0b01010,
        AIN11 = 0b01011,
        AIN12 = 0b01100,
        AIN13 = 0b01101,
        AIN14 = 0b01110,
        AIN15 = 0b01111,
        Temperature = 0b10000,
        AVSS = 0b10001,
        IntRef = 0b10010,
        DGND = 0b10011,
        /// (AVDD − AVSS)/6+. Use in conjunction with (AVDD − AVSS)/6− to monitor supply AVDD − AVSS.
        AVDD_DIV6_P = 0b10100,
        /// (AVDD − AVSS)/6−. Use in conjunction with (AVDD − AVSS)/6+ to monitor supply AVDD − AVSS.
        AVDD_DIV6_N = 0b10101,
        /// (IOVDD − DGND)/6+. Use in conjunction with (IOVDD − DGND)/6− to monitor IOVDD − DGND.
        IOVDD_DIV6_P = 0b10110,
        /// (IOVDD − DGND)/6−. Use in conjunction with (IOVDD − DGND)/6+ to monitor IOVDD − DGND.
        IOVDD_DIV6_N = 0b10111,
        /// (ALDO − AVSS)/6+. Use in conjunction with (ALDO − AVSS)/6− to monitor the analog LDO.
        ALDO_DIV6_P = 0b11000,
        /// (ALDO − AVSS)/6−. Use in conjunction with (ALDO − AVSS)/6+ to monitor the analog LDO.
        ALDO_DIV6_N = 0b11001,
        /// (DLDO − DGND)/6+. Use in conjunction with (DLDO − DGND)/6− to monitor the digital LDO.
        DLDO_DIV6_P = 0b11010,
        /// (DLDO − DGND)/6−. Use in conjunction with (DLDO − DGND)/6+ to monitor the digital LDO.
        DLDO_DIV6_N = 0b11011,
        /// V_20MV_P. Use in conjunction with V_20MV_M to apply a 20 mV p-p signal to the ADC.
        V_20MV_P = 0b11101,
        /// V_20MV_M. Use in conjunction with V_20MV_P to apply a 20 mV p-p signal to the ADC.
        V_20MV_N = 0b11110,
    }
}

register!{
    /// Channel Register
    ///
    /// Sixteen channel registers are included on the AD7124-8, CHANNEL_0 to CHANNEL_15. The channel registers begin at Address 0x09
    /// (CHANNEL_0) and end at Address 0x18 (CHANNEL_15). Via each register, the user can configure the channel (AINP input and AINM
    /// input), enable or disable the channel, and select the setup. The setup is selectable from eight different options defined by the user. When
    /// the ADC converts, it automatically sequences through all enabled channels. This allows the user to sample some channels multiple times
    /// in a sequence, if required. In addition, it allows the user to include diagnostic functions in a sequence also.
    [CHANNEL; 16](0x09 | rw): B16 {
        /// Channel enable bit. Setting this bit enables the device channel for the conversion sequence. By default, only the
        /// enable bit for Channel 0 is set. The order of conversions starts with the lowest enabled channel, then cycles
        /// through successively higher channel numbers, before wrapping around to the lowest channel again.
        /// When the ADC writes a result for a particular channel, the four LSBs of the status register are set to the channel
        /// number, 0 to 15. This allows the channel the data corresponds to be identified. When the DATA_STATUS bit in the
        /// ADC_CONTROL register is set, the contents of the status register are appended to each conversion when it is
        /// read. Use this function when several channels are enabled to determine to which channel the conversion value
        /// read corresponds.
        ENABLE: [15],
        /// Setup select. These bits identify which of the eight setups are used to configure the ADC for this channel. A setup
        /// comprises a set of four registers: analog configuration, output data rate/filter selection, offset register, and gain
        /// register. All channels can use the same setup, in which case the same 3-bit value must be written to these bits on
        /// all active channels. Alternatively, up to eight channels can be configured differently.
        SETUP: [14..12],
        /// Positive analog input AINP input select. These bits select which of the analog inputs is connected to the positive
        /// input for this channel.
        AINP(Input): [9..5],
        /// Negative analog input AINM input select. These bits select which of the analog inputs is connected to the
        /// negative input for this channel.
        AINM(Input): [4..0],
    }
}
// repeat

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    #[allow(non_camel_case_types)]
    pub enum Burnout {
        Off = 0b00,
        _0uA5 = 0b01,
        _2uA = 0b10,
        _4uA = 0b11,
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    #[allow(non_camel_case_types)]
    pub enum Ref {
        REFIN1 = 0b00,
        REFIN2 = 0b01,
        Internal = 0b10,
        AVDD = 0b11,
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    pub enum Gain {
        _1 = 0b000,
        _2 = 0b001,
        _4 = 0b010,
        _8 = 0b011,
        _16 = 0b100,
        _32 = 0b101,
        _64 = 0b110,
        _128 = 0b111,
    }
}

impl Into<u32> for Gain {
    fn into(self) -> u32 {
        match self {
            Gain::_1 => 1,
            Gain::_2 => 2,
            Gain::_4 => 4,
            Gain::_8 => 8,
            Gain::_16 => 16,
            Gain::_32 => 32,
            Gain::_64 => 64,
            Gain::_128 => 128,
        }
    }
}

register!{
    /// Configration Register
    ///
    /// The AD7124-8 has eight configuration registers, CONFIG_0 to CONFIG_7. Each configuration register is associated with a setup;
    /// CONFIG_x is associated with Setup x. In the configuration register, the reference source, polarity, reference buffers are configured. Table 75
    /// outlines the bit designations for the register. Bit 15 is the first bit of the data stream.
    [CONFIG; 8](0x19 | rw): B16 {
        /// Polarity select bit. When this bit is set, bipolar operation is selected. When this bit is cleared, unipolar operation is
        /// selected.
        BIPOLAR: [11],
        /// These bits select the magnitude of the sensor burnout detect current source.
        BURNOUT(Burnout): [10..9],
        /// Buffer enable on REFINx(+). When this bit is set, the positive reference input (internal or external) is buffered. When
        /// this bit is cleared, the positive reference input (internal or external) is unbuffered.
        REF_BUFP: [8],
        /// Buffer enable on REFINx(−). When this bit is set, the negative reference input (internal or external) is buffered. When
        /// this bit is cleared, the negative reference input (internal or external) is unbuffered.
        REF_BUFM: [7],
        /// Buffer enable on AINP. When this bit is set, the selected positive analog input pin is buffered. When this bit is cleared,
        /// the selected positive analog input pin is unbuffered.
        AIN_BUFP: [6],
        /// Buffer enable on AINM. When this bit is set, the selected negative analog input pin is buffered. When this bit is
        /// cleared, the selected negative analog input pin is unbuffered.
        AIN_BUFM: [5],
        /// Reference source select bits. These bits select the reference source to use when converting on any channels using
        /// this configuration register.
        REF_SEL(Ref): [4..3],
        /// Gain select bits. These bits select the gain to use when converting on any channels using this configuration register.
        PGA(Gain): [2..0],
    }
}

register_enum!{
    #[repr(u8)]
    #[derive(Debug, PartialEq, Copy, Clone)]
    pub enum Filter {
        /// sinc4 filter (default)
        Sinc4 = 0b000,
        __Reserved1 = 0b001,
        /// sinc3 filter.
        Sinc3 = 0b010,
        __Reserved2 = 0b011,
        /// Fast settling filter using the sinc4 filter. The sinc4 filter is followed by an averaging block, which results
        /// in a settling time equal to the conversion time. In full power and mid power modes, averaging by 16 occurs
        /// whereas averaging by 8 occurs in low power mode.
        FastSettlingSinc4 = 0b100,
        /// fast settling filter using the sinc3 filter. The sinc3 filter is followed by an averaging block, which results
        /// in a settling time equal to the conversion time. In full power and mid power modes, averaging by 16 occurs
        /// whereas averaging by 8 occurs in low power mode.
        FastSettlingSinc3 = 0b101,
        __Reserved3 = 0b110,
        /// Post filter enabled. The AD7124-8 includes several post filters, selectable using the POST_FILTER bits.
        /// The post filters have single cycle settling, the settling time being considerably better than a simple
        /// sinc3/sinc4 filter. These filters offer excellent 50 Hz and60 Hz rejection.
        PostFilterEn = 0b111,
    }
}

register!{
    /// The AD7124-8 has eight filter registers, FILTER_0 to FILTER_7. Each filter register is associated with a setup; FILTER_x is associated
    /// with Setup x. In the filter register, the filter type and output word rate are set.
    [FILTER; 8](0x21 | rw): B24 {
        /// Filter type select bits. These bits select the filter type.
        FILTER(Filter): [23..21],
        /// When this bit is set, a first order notch is placed at 60 Hz when the first notch of the sinc filter is at 50 Hz.
        /// This allows simultaneous 50 Hz and 60 Hz rejection.
        REJ60: [20],
        /// Post filter type select bits. When the filter bits are set to 1, the sinc3 filter is followed by a post filter that
        /// offers good 50 Hz and 60 Hz rejection at output data rates that have zero latency approximately.
        POST_FILTER: [19..17],
        /// Single cycle conversion enable bit. When this bit is set, the AD7124-8 settles in one conversion cycle so that
        /// it functions as a zero latency ADC. This bit has no effect when multiple analog input channels are enabled
        /// or when the single conversion mode is selected. When the fast filters are used, this bit has no effect.
        SINGLE_CYCLE: [16],
        /// Filter output data rate select bits. These bits set the output data rate of the sinc3 filter, sinc4 filter, and fast
        /// settling filters. In addition, they affect the position of the first notch of the sinc filter and the cutoff
        /// frequency. In association with the gain selection, they also determine the output noise and, therefore, the
        /// effective resolution of the device (see noise tables). FS can have a value from 1 to 2047.
        FS: [10..0],
    }
}

register!{
    /// The AD7124-8 has eight offset registers, OFFSET_0 to OFFSET_7.
    /// Each offset register is associated with a setup; OFFSET_x is
    /// associated with Setup x. The offset registers are 24-bit registers
    /// and hold the offset calibration coefficient for the ADC and its
    /// power-on reset value is 0x800000. Each of these registers is a
    /// read/write register. These registers are used in conjunction with
    /// the associated gain register to form a register pair. The power-
    /// on reset value is automatically overwritten if an internal or
    /// system zero-scale calibration is initiated by the user. The ADC
    /// must be placed in standby mode or idle mode when writing to
    /// the offset registers.
    [OFFSET; 8](0x29 | rw): B24 {
        VALUE: [23..0],
    }
}

register!{
    /// The AD7124-8 has eight gain registers, GAIN_0 to GAIN_7. Each
    /// gain register is associated with a setup; GAIN_x is associated
    /// with Setup x. The gain registers are 24-bit registers and hold the
    /// full-scale calibration coefficient for the ADC. The AD7124-8 is
    /// factory calibrated to a gain of 1. The gain register contains this
    /// factory generated value on power-on and after a reset. The gain
    /// registers are read/write registers. However, when writing to the
    /// registers, the ADC must be placed in standby mode or idle
    /// mode. The default value is automatically overwritten if an
    /// internal or system full-scale calibration is initiated by the user
    /// or the full-scale registers are written to.
    [GAIN; 8](0x31 | rw): B24 {
        VALUE: [23..0],
    }
}
