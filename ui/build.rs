use std::env;
use std::path::PathBuf;
use cmake::Config;

fn main() {
    // Generate Rust binding for the UI library
    println!("cargo:rerun-if-changed=src/wrapper.h");
    let bindings = bindgen::Builder::default()
        .header("src/wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");


    // Compile ui library
    let dst = Config::new("../ui")
        .define("CMAKE_TOOLCHAIN_FILE", "../remote/arm-none-eabi.cmake")
        .always_configure(true)
        .build();

    println!("cargo:rustc-link-search=native={}/build", dst.display());
    println!("cargo:rustc-link-lib=static=ui");

    println!("cargo:rustc-link-search=native={}/build/lib", dst.display());
    println!("cargo:rustc-link-lib=static=lvgl");

    println!("cargo:rustc-link-search=native={}/build/lib/lvgl", dst.display());
    println!("cargo:rustc-link-lib=static=lvgl_demos");

    //println!("cargo:rustc-link-search=native=/usr/arm-none-eabi/lib");
    //println!("cargo:rustc-link-lib=static=c_nano");
    //println!("cargo:rustc-link-lib=static=nosys");

    // list most important files to rerun build manually
    println!("cargo:rerun-if-changed=src/ui.c");
    println!("cargo:rerun-if-changed=src/ui.h");
    println!("cargo:rerun-if-changed=src/view_model.h");
    println!("cargo:rerun-if-changed=src/wrapper.h");
    println!("cargo:rerun-if-changed=src/port/stm32f7/lv_conf.h");
    println!("cargo:rerun-if-changed=src/port/stm32f7/lcd.c");
}