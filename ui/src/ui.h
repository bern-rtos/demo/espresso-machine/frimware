#ifndef UI_H
#define UI_H

#include "view_model.h"

void UI_init(void);
void UI_update(const MODEL_view_model_t *model);
void UI_tick(void);

#endif // UI_H
