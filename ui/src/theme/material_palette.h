#ifndef MATERIAL_PALATTE_H
#define MATERIAL_PALATTE_H

#include "lv_theme_espresso.h"

// blue, purple
//#define MATERIAL_COLOR_PRIMARY                lv_color_hex(DISP_COLOR_MAP(0x5cd5fa))
//#define MATERIAL_COLOR_ON_PRIMARY             lv_color_hex(DISP_COLOR_MAP(0x003543))
//#define MATERIAL_COLOR_PRIMARY_CONTAINER      lv_color_hex(DISP_COLOR_MAP(0x004e61))
//#define MATERIAL_COLOR_ON_PRIMARY_CONTAINER   lv_color_hex(DISP_COLOR_MAP(0xb2ebff))
//#define MATERIAL_COLOR_SECONDARY              lv_color_hex(DISP_COLOR_MAP(0xb3cad4))
//#define MATERIAL_COLOR_ON_SECONDARY           lv_color_hex(DISP_COLOR_MAP(0x1d333b))
//#define MATERIAL_COLOR_SECONDARY_CONTAINER    lv_color_hex(DISP_COLOR_MAP(0x344a52))
//#define MATERIAL_COLOR_ON_SECONDARY_CONTAINER lv_color_hex(DISP_COLOR_MAP(0xcee6f0))
//#define MATERIAL_COLOR_TERTIARY               lv_color_hex(DISP_COLOR_MAP(0xc2c3eb))
//#define MATERIAL_COLOR_ON_TERTIARY            lv_color_hex(DISP_COLOR_MAP(0x2b2e4d))
//#define MATERIAL_COLOR_TERTIARY_CONTAINER     lv_color_hex(DISP_COLOR_MAP(0x424465))
//#define MATERIAL_COLOR_ON_TERTIARY_CONTAINER  lv_color_hex(DISP_COLOR_MAP(0xe0e0ff))
//#define MATERIAL_COLOR_ERROR                  lv_color_hex(DISP_COLOR_MAP(0xffb4a9))
//#define MATERIAL_COLOR_ON_ERROR               lv_color_hex(DISP_COLOR_MAP(0x680003))
//#define MATERIAL_COLOR_ERROR_CONTAINER        lv_color_hex(DISP_COLOR_MAP(0x930006))
//#define MATERIAL_COLOR_ON_ERROR_CONTAINER     lv_color_hex(DISP_COLOR_MAP(0xffdad4))
//#define MATERIAL_COLOR_BACKGROUND             lv_color_hex(DISP_COLOR_MAP(0x191c1d))
//#define MATERIAL_COLOR_ON_BACKGROUND          lv_color_hex(DISP_COLOR_MAP(0xe1e3e4))
//#define MATERIAL_COLOR_SURFACE                lv_color_hex(DISP_COLOR_MAP(0x191c1d))
//#define MATERIAL_COLOR_ON_SURFACE             lv_color_hex(DISP_COLOR_MAP(0xe1e3e4))
//#define MATERIAL_COLOR_OUTLINE                lv_color_hex(DISP_COLOR_MAP(0x899296))
//#define MATERIAL_COLOR_SURFACE_VARIANT        lv_color_hex(DISP_COLOR_MAP(0x40484b))
//#define MATERIAL_COLOR_ON_SURFACE_VARIANT     lv_color_hex(DISP_COLOR_MAP(0xbfc8cc))


// yellow, green
#define MATERIAL_COLOR_PRIMARY                lv_color_hex(DISP_COLOR_MAP(0xefc148))
#define MATERIAL_COLOR_ON_PRIMARY             lv_color_hex(DISP_COLOR_MAP(0x3f2e00))
#define MATERIAL_COLOR_PRIMARY_CONTAINER      lv_color_hex(DISP_COLOR_MAP(0x5a4300))
#define MATERIAL_COLOR_ON_PRIMARY_CONTAINER   lv_color_hex(DISP_COLOR_MAP(0xffdf90))
#define MATERIAL_COLOR_SECONDARY              lv_color_hex(DISP_COLOR_MAP(0xd7c5a1))
#define MATERIAL_COLOR_ON_SECONDARY           lv_color_hex(DISP_COLOR_MAP(0x3a2f15))
#define MATERIAL_COLOR_SECONDARY_CONTAINER    lv_color_hex(DISP_COLOR_MAP(0x514529))
#define MATERIAL_COLOR_ON_SECONDARY_CONTAINER lv_color_hex(DISP_COLOR_MAP(0xf4e1bb))
#define MATERIAL_COLOR_TERTIARY               lv_color_hex(DISP_COLOR_MAP(0xafcfac))
#define MATERIAL_COLOR_ON_TERTIARY            lv_color_hex(DISP_COLOR_MAP(0x1b361d))
#define MATERIAL_COLOR_TERTIARY_CONTAINER     lv_color_hex(DISP_COLOR_MAP(0x314d32))
#define MATERIAL_COLOR_ON_TERTIARY_CONTAINER  lv_color_hex(DISP_COLOR_MAP(0xcaebc6))
#define MATERIAL_COLOR_ERROR                  lv_color_hex(DISP_COLOR_MAP(0xffb4a9))
#define MATERIAL_COLOR_ON_ERROR               lv_color_hex(DISP_COLOR_MAP(0x680003))
#define MATERIAL_COLOR_ERROR_CONTAINER        lv_color_hex(DISP_COLOR_MAP(0x930006))
#define MATERIAL_COLOR_ON_ERROR_CONTAINER     lv_color_hex(DISP_COLOR_MAP(0xffdad4))
#define MATERIAL_COLOR_BACKGROUND             lv_color_hex(DISP_COLOR_MAP(0x1e1b16))
#define MATERIAL_COLOR_ON_BACKGROUND          lv_color_hex(DISP_COLOR_MAP(0xe8e1d8))
#define MATERIAL_COLOR_SURFACE                lv_color_hex(DISP_COLOR_MAP(0x1e1b16))
#define MATERIAL_COLOR_ON_SURFACE             lv_color_hex(DISP_COLOR_MAP(0xe8e1d8))
#define MATERIAL_COLOR_OUTLINE                lv_color_hex(DISP_COLOR_MAP(0x999080))
#define MATERIAL_COLOR_SURFACE_VARIANT        lv_color_hex(DISP_COLOR_MAP(0x4c4639))
#define MATERIAL_COLOR_ON_SURFACE_VARIANT     lv_color_hex(DISP_COLOR_MAP(0xcfc5b3))

#endif //MATERIAL_PALATTE_H
