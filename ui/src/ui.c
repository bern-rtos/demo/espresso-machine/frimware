#include "ui.h"

#include "lvgl/lvgl.h"
#include "theme/material_palette.h"
#include "lcd.h"

static lv_obj_t *machine_lbl_pressure;
static lv_obj_t *machine_arc_pressure;
static lv_obj_t *machine_lbl_temp_boiler_inside;
static lv_obj_t *machine_lbl_temp_boiler_outside;
static lv_obj_t *machine_lbl_temp_group_head_outside;
static lv_obj_t *machine_lbl_volume;
static lv_obj_t *machine_lbl_time;
static lv_obj_t *machine_arc_time;

#define UI_FORMAT_FLOAT_DEC1(val) ((int)val), ((int)(val * 10 - (int)val * 10))

static void create_ui(void);

void UI_init(void) {
    lv_init();
    LCD_init();
    create_ui();
}

static void create_ui(void) {
    //lv_demo_benchmark();

    static lv_style_t sty_text_on_bg;
    lv_style_init(&sty_text_on_bg);
    lv_style_set_text_color(&sty_text_on_bg, MATERIAL_COLOR_ON_BACKGROUND);

    static lv_style_t sty_text_on_primary;
    lv_style_init(&sty_text_on_primary);
    lv_style_set_text_color(&sty_text_on_primary, MATERIAL_COLOR_ON_PRIMARY);

    static lv_style_t sty_text_on_surface_var;
    lv_style_init(&sty_text_on_surface_var);
    lv_style_set_text_color(&sty_text_on_surface_var, MATERIAL_COLOR_ON_SURFACE_VARIANT);

    lv_obj_t *tabview = lv_tabview_create(lv_scr_act(), LV_DIR_TOP, 60);
    lv_obj_t *tab_simple = lv_tabview_add_tab(tabview, "Overview");

    // image overlay container
    lv_obj_t *cont_overlay = lv_obj_create(tab_simple);
    lv_obj_remove_style_all(cont_overlay);
    lv_obj_set_size(cont_overlay, 325, 510);
    lv_obj_align(cont_overlay, LV_ALIGN_TOP_MID, 0, 30);

    // bg image
    LV_IMG_DECLARE(ui_shot);
    lv_obj_t *machine = lv_img_create(cont_overlay);
    lv_img_set_src(machine, &ui_shot);
    lv_obj_align(machine, LV_ALIGN_TOP_LEFT, 0, 0);

    // pressure gauge
    machine_arc_pressure = lv_arc_create(lv_scr_act());
    lv_arc_set_rotation(machine_arc_pressure, 160);
    lv_arc_set_bg_angles(machine_arc_pressure, 0, 220);
    lv_obj_remove_style(machine_arc_pressure, NULL, LV_PART_KNOB);
    lv_obj_clear_flag(machine_arc_pressure, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_arc_width(machine_arc_pressure, 15, LV_PART_MAIN);
    lv_obj_set_style_arc_width(machine_arc_pressure, 15, LV_PART_INDICATOR);
    lv_obj_set_size(machine_arc_pressure, 150, 150);
    lv_obj_align(machine_arc_pressure, LV_ALIGN_TOP_LEFT, 75, 150);

    machine_lbl_pressure = lv_label_create(machine_arc_pressure);
    lv_label_set_text(machine_lbl_pressure, "9.2bar");
    lv_obj_add_style(machine_lbl_pressure, &sty_text_on_bg, 0);
    lv_obj_align(machine_lbl_pressure, LV_ALIGN_CENTER, 0, 0);

    // boiler temp label
    machine_lbl_temp_boiler_inside = lv_label_create(cont_overlay);
    lv_label_set_text(machine_lbl_temp_boiler_inside, "96.2°C");
    lv_obj_add_style(machine_lbl_temp_boiler_inside, &sty_text_on_surface_var, 0);
    lv_obj_align(machine_lbl_temp_boiler_inside, LV_ALIGN_TOP_LEFT, 210, 100);

    // boiler outside temp label
    machine_lbl_temp_boiler_outside = lv_label_create(cont_overlay);
    lv_label_set_text(machine_lbl_temp_boiler_outside, "94.2°C");
    lv_obj_add_style(machine_lbl_temp_boiler_outside, &sty_text_on_surface_var, 0);
    lv_obj_align(machine_lbl_temp_boiler_outside, LV_ALIGN_TOP_LEFT, 210, 20);


    // group head outside temp label
    machine_lbl_temp_group_head_outside = lv_label_create(cont_overlay);
    lv_label_set_text(machine_lbl_temp_group_head_outside, "72.2°C");
    lv_obj_add_style(machine_lbl_temp_group_head_outside, &sty_text_on_surface_var, 0);
    lv_obj_align(machine_lbl_temp_group_head_outside, LV_ALIGN_TOP_LEFT, 210, 240);

    // volume label
    machine_lbl_volume = lv_label_create(cont_overlay);
    lv_label_set_text(machine_lbl_volume, "24ml");
    lv_obj_add_style(machine_lbl_volume, &sty_text_on_surface_var, 0);
    lv_obj_align(machine_lbl_volume, LV_ALIGN_TOP_LEFT, 215, 425);

    // time label
    machine_arc_time = lv_arc_create(lv_scr_act());
    lv_arc_set_rotation(machine_arc_time, 270);
    lv_arc_set_bg_angles(machine_arc_time, 0, 360);
    lv_obj_remove_style(machine_arc_time, NULL, LV_PART_KNOB);
    lv_obj_clear_flag(machine_arc_time, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_arc_width(machine_arc_time, 15, LV_PART_MAIN);
    lv_obj_set_style_arc_width(machine_arc_time, 15, LV_PART_INDICATOR);
    lv_obj_set_size(machine_arc_time, 150, 150);
    lv_obj_align(machine_arc_time, LV_ALIGN_TOP_LEFT, 75, 477);

    machine_lbl_time = lv_label_create(machine_arc_time);
    lv_label_set_text(machine_lbl_time, "5s");
    lv_obj_align(machine_lbl_time, LV_ALIGN_CENTER, 0, 0);

}

void UI_update(const MODEL_view_model_t *model) {
    //lv_meter_set_indicator_end_value(machine_arc_pressure, machine_indic_pressure, model->pressure_water*10.0);
    lv_label_set_text_fmt(machine_lbl_time, "%ds", (int)model->time);
    lv_arc_set_value(machine_arc_time, (int)(model->time / 30.0f * 100.0f));

    // float does not seem to work on embedded target
    lv_label_set_text_fmt(machine_lbl_pressure, "%d.%dbar", UI_FORMAT_FLOAT_DEC1(model->pressure_water));
    lv_arc_set_value(machine_arc_pressure, (int)(model->pressure_water / 15.0f * 100.0f));

    lv_label_set_text_fmt(machine_lbl_temp_boiler_inside, "%d.%d°C", UI_FORMAT_FLOAT_DEC1(model->temperature.boiler_inside));
    lv_label_set_text_fmt(machine_lbl_temp_boiler_outside, "%d.%d°C", UI_FORMAT_FLOAT_DEC1(model->temperature.boiler_outside));
    lv_label_set_text_fmt(machine_lbl_temp_group_head_outside, "%d.%d°C", UI_FORMAT_FLOAT_DEC1(model->temperature.group_head_outside));
    lv_label_set_text_fmt(machine_lbl_volume, "%dml", (int)model->volume_water);
}

void UI_tick(void) {
    lv_tick_inc(5);
    lv_task_handler();
}