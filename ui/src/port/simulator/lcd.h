#ifndef LCD_H
#define LCD_H

#define LCD_RES_H (800)
#define LCD_RES_V (480)

void LCD_init(void);

#endif // LCD_H
