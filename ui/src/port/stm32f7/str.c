#include <stddef.h>

char *strcpy(char *d, const char *s){
    char *saved = d;
    while ((*d++ = *s++) != '\0');

    return saved; //returning starting address of s1
}

size_t strlen(const char *str){
    const char *s;

    for (s = str; *s; ++s);

    return (s - str);
}

int strcmp(const char* s1, const char* s2) {
    while(*s1 && (*s1 == *s2)) {
        s1++;
        s2++;
    }
    return *(const unsigned char*)s1 - *(const unsigned char*)s2;
}