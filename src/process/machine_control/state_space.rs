mod math;
use math::linalg;

#[allow(non_snake_case)]
#[derive(Copy, Clone)]
pub struct System<const N_X: usize, const N_Y: usize, const N_U: usize> {
    /// Transition matrix
    pub A: [[f32; N_X]; N_X],
    pub B: [[f32; N_U]; N_X],
    pub C: [[f32; N_X]; N_Y],
    pub D: [[f32; N_Y]; N_U],
}

pub struct Controller<const N_X: usize, const N_U: usize> {
    /// Kalman gain
    k: [[f32; N_X]; N_U],
    /// Set point
    r: [f32; N_U],
    /// Set point gain
    k_r: [f32; N_U],
}

impl<const N_X: usize, const N_U: usize> Controller<{ N_X }, { N_U }> {
    pub const fn new(k: [[f32; N_X]; N_U], r: [f32; N_U], k_r: [f32; N_U]) -> Self {
        Controller {
            k,
            r,
            k_r,
        }
    }

    #[allow(unused)]
    pub fn set_reference(&mut self, r: [f32; N_U]) {
        for (i, v) in self.r.iter_mut().enumerate() {
            *v = r[i];
        }
    }

    pub fn update(&self, x: &[f32; N_X], u: &mut [f32; N_U]) {
        let mut r_scaled = [0.0_f32; N_U];
        for (i, v) in r_scaled.iter_mut().enumerate() {
            *v = self.r[i];
        }

        linalg::vector_times_vector(&mut r_scaled, &self.k_r);
        linalg::matrix_times_vector(&self.k, x, u);
        linalg::vector_negate(u);
        linalg::vector_plus_vector(u, &r_scaled);
    }
}

#[allow(non_snake_case)]
pub struct Observer<const N_X: usize, const N_Y: usize, const N_U: usize> {
    system: System<{ N_X }, { N_Y }, { N_U }>,
    /// Estimated state vector
    x: [f32; N_X],
    /// Luenberger matrix
    H: [[f32; N_Y]; N_X],
    /// Time step
    dt: f32,
}

impl<const N_X: usize, const N_Y: usize, const N_U: usize> Observer<{ N_X }, { N_Y }, { N_U }> {
    #[allow(non_snake_case)]
    pub const fn new(system: System<N_X, N_Y, N_U>, H: [[f32; N_Y]; N_X], dt: f32) -> Self {
        Observer {
            system,
            x: [0.0_f32; N_X],
            H,
            dt,
        }
    }

    pub fn init_state(&mut self, x_0: [f32; N_X]) {
        for (i, v) in self.x.iter_mut().enumerate() {
            *v = x_0[i];
        }
    }

    pub fn update(&mut self, u: &[f32; N_U], y: &[f32; N_Y]) {
        let mut y = *y;
        let mut dx = [0.0_f32; N_X];
        let mut y_est = [0.0_f32; N_Y];
        let mut dx_temp = [0.0_f32; N_X];

        // B * u
        linalg::matrix_times_vector(&self.system.B, u, &mut dx);

        // H * (y - C * e_est)
        linalg::matrix_times_vector(&self.system.C, &self.x, &mut y_est);
        linalg::vector_minus_vector(&mut y, &y_est);
        linalg::matrix_times_vector(&self.H, &y, &mut dx_temp);
        linalg::vector_plus_vector(&mut dx, &dx_temp);

        // A * e_est
        linalg::matrix_times_vector(&self.system.A, &self.x, &mut dx_temp);
        linalg::vector_plus_vector(&mut dx, &dx_temp);
        linalg::vector_times_scalar(&mut dx, self.dt);

        linalg::vector_plus_vector(&mut self.x, &dx);
    }

    pub fn x(&self) -> [f32; N_X] {
        self.x
    }
}