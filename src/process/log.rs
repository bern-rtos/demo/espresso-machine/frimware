use alloc::sync::Arc;
use core::fmt::Write;

use bern_kernel::exec::process::{Process, ProcessError};
use bern_kernel::exec::runnable::Priority;
use bern_kernel::exec::thread::Thread;
use bern_kernel::stack::Stack;
use bern_kernel::sync::Semaphore;
use bern_kernel::time::SysClock;
use bern_kernel::embedded_time::clock::Clock;
use bern_kernel::exec::interrupt::{InterruptHandler, InterruptStack};
use bern_kernel::mem::queue::spsc_const::ConstQueue;
use bern_kernel::sync::ipc::channel::IpcReceiver;
use bern_kernel::log::{debug, warn, error};
use bern_kernel::sleep;

use board::ethernet::Ethernet;


use heapless::String;

use smoltcp::iface::SocketHandle;
use smoltcp::socket::TcpSocket;
use smoltcp::wire::{IpEndpoint, Ipv4Address, Ipv4Cidr};
use smoltcp::time::Instant;
use smoltcp::wire::IpAddress;
use crate::process::machine_control::{ControllerData, MeasData};

static LOG: &Process = bern_kernel::new_process!(log, 32768);

pub struct Peripherals {
    pub ethernet: Ethernet,
}

pub struct Ipc<const N: usize> {
    pub log_receiver: IpcReceiver<ConstQueue<MeasData, { N }>>,
    pub log_ctrl_receiver: IpcReceiver<ConstQueue<ControllerData, { N }>>,
}

#[link_section = ".process.log"]
pub static LOG_QUEUE: ConstQueue<MeasData, 16> = ConstQueue::new();

#[link_section = ".process.log"]
pub static LOG_CTRL_QUEUE: ConstQueue<ControllerData, 16> = ConstQueue::new();


const N_TCP_SOCKETS: usize = 1;

pub struct NetStorage {
    pub ip_addrs: [smoltcp::wire::IpCidr; 1],

    // Note: There is an additional socket set item required for the DHCP socket.
    pub sockets: [smoltcp::iface::SocketStorage<'static>; N_TCP_SOCKETS],
    pub tcp_socket_storage: [TcpSocketStorage; N_TCP_SOCKETS],
    pub neighbor_cache: [Option<(IpAddress, smoltcp::iface::Neighbor)>; 8],
    pub routes_cache: [Option<(smoltcp::wire::IpCidr, smoltcp::iface::Route)>; 8],
}

#[derive(Copy, Clone)]
pub struct TcpSocketStorage {
    rx_storage: [u8; 1024],
    tx_storage: [u8; 1024],
}

impl TcpSocketStorage {
    const fn new() -> Self {
        Self {
            rx_storage: [0; 1024],
            tx_storage: [0; 1024],
        }
    }
}

#[link_section = ".process.log"]
static mut NETWORK_STORAGE: NetStorage = NetStorage {
    ip_addrs: [smoltcp::wire::IpCidr::Ipv4(Ipv4Cidr::new(Ipv4Address([0; 4]), 24))],
    neighbor_cache: [None; 8],
    routes_cache: [None; 8],
    sockets: [smoltcp::iface::SocketStorage::EMPTY; N_TCP_SOCKETS],
    tcp_socket_storage: [TcpSocketStorage::new(); N_TCP_SOCKETS],
};


struct NetConfig {
    pub ip: IpAddress,
    pub netmask: u8,
    pub mac: smoltcp::wire::EthernetAddress,
}

const NETCONFIG: NetConfig = NetConfig {
    ip: IpAddress::Ipv4(Ipv4Address::new(192, 168, 10, 90)),
    netmask: 24,
    mac: smoltcp::wire::EthernetAddress([0x00, 0x00, 0xDE, 0xAD, 0xBE, 0xEF]),
};

pub fn init<const N: usize>(peripherals: Peripherals, ipc: Ipc<{ N }>) -> Result<(), ProcessError> {
    let mut eth = peripherals.ethernet;
    let log_receiver = ipc.log_receiver;
    let log_ctrl_receiver = ipc.log_ctrl_receiver;

    LOG.init(move |c| {
        eth.enable();
        let eth_irqn = eth.irqn();
        let mut eth = eth.eth;

        let eth_pending = Arc::new(Semaphore::new(0));
        let eth_pending_recv = eth_pending.clone();

        Thread::new(c)
            .priority(Priority::new(3))
            .stack(Stack::try_new_in(c, 16384).unwrap())
            .name("Network Log\0")
            .spawn(move || {
                let clock = SysClock::<u32>::new();

                let store = unsafe { &mut NETWORK_STORAGE };
                store.ip_addrs[0] = smoltcp::wire::IpCidr::new(NETCONFIG.ip, NETCONFIG.netmask);

                let mut routes = smoltcp::iface::Routes::new(&mut store.routes_cache[..]);
                routes
                    .add_default_ipv4_route(Ipv4Address::UNSPECIFIED)
                    .unwrap();

                let neighbor_cache = smoltcp::iface::NeighborCache::new(&mut store.neighbor_cache[..]);

                let mut iface = smoltcp::iface::InterfaceBuilder::new(
                    &mut eth,
                    &mut store.sockets[..],
                )
                    .hardware_addr(smoltcp::wire::HardwareAddress::Ethernet(NETCONFIG.mac))
                    .neighbor_cache(neighbor_cache)
                    .ip_addrs(&mut store.ip_addrs[..])
                    .routes(routes)
                    .finalize();

                let mut sockets_tcp = [SocketHandle::default(); N_TCP_SOCKETS];
                for (i, storage) in store.tcp_socket_storage[..].iter_mut().enumerate() {
                    let tcp_socket = {
                        let rx_buffer = smoltcp::socket::TcpSocketBuffer::new(
                            &mut storage.rx_storage[..],
                        );
                        let tx_buffer = smoltcp::socket::TcpSocketBuffer::new(
                            &mut storage.tx_storage[..],
                        );

                        TcpSocket::new(rx_buffer, tx_buffer)
                    };

                    sockets_tcp[i] = iface.add_socket(tcp_socket);
                }

                let telegraf_socket = sockets_tcp[0];

                let mut tcp_active = false;
                loop {
                    let (socket, cx) = iface.get_socket_and_context::<TcpSocket>(telegraf_socket);

                    if !socket.is_open() {
                        socket.connect(cx,
                                       IpEndpoint::new(IpAddress::Ipv4(Ipv4Address::new(192, 168, 10, 10)), 8094),
                                       IpEndpoint::new(NETCONFIG.ip, 1235),
                        ).unwrap();
                    }

                    let now_ms = clock.try_now().unwrap().duration_since_epoch().integer();
                    match iface.poll(Instant::from_millis(now_ms)) {
                        Ok(_) => {}
                        Err(e) => {
                            debug!("Eth poll error: {}", e);
                        }
                    }

                    let socket = iface.get_socket::<TcpSocket>(telegraf_socket);
                    if socket.is_active() && !tcp_active {
                        debug!("Connected");
                    } else if !socket.is_active() && tcp_active {
                        debug!("Disconnected");
                    }
                    tcp_active = socket.is_active();

                    if socket.may_send() {
                        log_receiver.recv().map(|d| {
                            let ser: String<256> = serde_json_core::ser::to_string(&d).unwrap();
                            socket.write_str(ser.as_str())
                                .map(|_| socket.close())
                                .or_else(|e| {
                                    warn!("TCP send error: {:?}", e);
                                    Err(e)
                                })
                                .unwrap_or_else(|_| error!("Socket was closed"));
                        }).ok();
                    }
                    if socket.may_send() {
                        log_ctrl_receiver.recv().map(|d| {
                            let ser: String<256> = serde_json_core::ser::to_string(&d).unwrap();
                            socket.write_str(ser.as_str())
                                .map(|_| socket.close())
                                .or_else(|e| {
                                    warn!("TCP send error: {:?}", e);
                                    Err(e)
                                })
                                .unwrap_or_else(|_| error!("Socket was closed"));
                        }).ok();
                    }
                    match eth_pending_recv.try_acquire() {
                        Ok(p) => p.forget(),
                        Err(_) => sleep(20),
                    }
                }
            });

        InterruptHandler::new(c)
            .stack(InterruptStack::Kernel)
            .connect_interrupt(eth_irqn)
            .handler(move |_c| {
                unsafe {
                    board::ethernet::handle_interrupt();
                }
                eth_pending.add_permits(1);
            });
    })
}