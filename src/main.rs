#![no_std]
#![no_main]
#![feature(type_ascription)]

extern crate alloc;

mod process;
mod log;

use core::panic::PanicInfo;

use bern_kernel::sync;
use bern_kernel::units::frequency::ExtMilliHertz;

use cortex_m_rt::{entry, exception, ExceptionFrame};
use board::Board;

use process::log::{LOG_QUEUE, LOG_CTRL_QUEUE};
use crate::process::ui::{UI_MEAS_QUEUE, UI_SUSPEND_QUEUE};

#[entry]
fn main() -> ! {
    let board = Board::new();

    log::init();

    bern_kernel::init();
    bern_kernel::time::set_tick_frequency(
        1.kHz(),
        board::SYSCLK.to_Hz().Hz()
    );


    let log_channel = sync::ipc::spsc::channel();
    let (log_sender, log_receiver) = log_channel.split(&LOG_QUEUE).unwrap();

    let ctrl_channel = sync::ipc::spsc::channel();
    let (log_ctrl_sender, log_ctrl_receiver) = ctrl_channel.split(&LOG_CTRL_QUEUE).unwrap();

    let ui_channel = sync::ipc::spsc::channel();
    let (ui_sender, ui_receiver) = ui_channel.split(&UI_MEAS_QUEUE).unwrap();
    let ui_susp_channel = sync::ipc::spsc::channel();
    let (ui_suspend_sender, ui_susp_receiver) = ui_susp_channel.split(&UI_SUSPEND_QUEUE).unwrap();

    let sense_periph = process::machine_control::Peripherals {
        actuators: board.actuators,
        buttons: board.buttons,
        state: board.state,
        spi: board.sense_bus,
        analog_sensors: board.analog_sensors,
        flow_sensor: board.flow_sensor,
    };
    let machine_ipc = process::machine_control::Ipc {
        log_sender,
        log_ctrl_sender,
        ui_sender,
        ui_suspend_sender,
    };
    process::machine_control::init(sense_periph, machine_ipc).unwrap();


    let log_periph = process::log::Peripherals {
        ethernet: board.ethernet,
    };
    let log_ipc = process::log::Ipc {
        log_receiver,
        log_ctrl_receiver,
    };
    process::log::init(log_periph, log_ipc).unwrap();


    let ui_peripherals = process::ui::Peripherals {
        display: board.display,
    };
    let ui_ipc = process::ui::Ipc {
        meas_receiver: ui_receiver,
        suspend_receiver: ui_susp_receiver,
    };
    process::ui::init(ui_peripherals, ui_ipc).unwrap();


    bern_kernel::start()
}

#[panic_handler]
fn panic_handler(_info: &PanicInfo) -> ! {
    board::emergency_stop();
    cortex_m::peripheral::SCB::sys_reset();
}

#[allow(non_snake_case)]
#[exception]
unsafe fn HardFault(_ef: &ExceptionFrame) -> ! {
    board::emergency_stop();
    cortex_m::peripheral::SCB::sys_reset();
}


