#[cfg(all(feature = "log-systemview", any(feature = "log-rtt", feature = "log-defmt")))]
compile_error!("You can only select one logger.");
#[cfg(all(feature = "log-rtt", any(feature = "log-systemview", feature = "log-defmt")))]
compile_error!("You can only select one logger.");
#[cfg(all(feature = "log-defmt", any(feature = "log-rtt", feature = "log-systemview")))]
compile_error!("You can only select one logger.");

#[cfg(feature = "log-systemview")]
mod systemview;
#[cfg(feature = "log-rtt")]
mod rtt_global;
#[cfg(feature = "log-defmt")]
use defmt_rtt as _;

#[cfg(feature = "log-systemview")]
static LOGGER: systemview_target::SystemView = systemview_target::SystemView::new();
#[cfg(feature = "log-rtt")]
static LOGGER: rtt_global::RttLogger = rtt_global::RttLogger::new();

#[cfg(any(feature = "log-systemview", feature = "log-rtt"))]
use log::LevelFilter;

pub fn init() {
    #[cfg(any(feature = "log-systemview", feature = "log-rtt"))]
    {
        LOGGER.init();
        log::set_logger(&LOGGER).ok();
        log::set_max_level(LevelFilter::Trace);
    }
}