// Why no traits?
//
// Functions need to be `const` so that they can be evaluated at compile-time.
// However, `const trait` implementation are not stable yet.

macro_rules! impl_reg_type {
    ($name:ident, $repr:ty, $size:literal) => {
        #[allow(non_snake_case)]
        pub mod $name {
            use core::ops::{BitAnd, BitOr, Not, Shr, Shl};

            pub type Repr = $repr;
            pub const BYTES: usize = $size;

            #[derive(Copy, Clone)]
            pub struct $name {
                raw: $repr,
            }

            impl $name {
                pub const fn mask(msb: $repr, lsb: $repr) -> $name {
                    $name::new((((1u64 << (msb - lsb + 1)) - 1) << lsb) as $repr)
                }

                pub const fn new(bits: $repr) -> Self {
                    $name {
                        raw: bits
                    }
                }

                #[inline(always)]
                pub fn raw(&self) -> $repr {
                    self.raw
                }
            }

            impl From<$repr> for $name {
                fn from(value: $repr) -> $name {
                    $name::new(value)
                }
            }

            impl From<bool> for $name {
                fn from(value: bool) -> Self {
                    $name::new(value as $repr)
                }
            }

            impl From<$name> for $repr {
                fn from(value: $name) -> Self {
                    value.raw()
                }
            }

            impl From<$name> for bool {
                fn from(value: $name) -> Self {
                    value.raw() > 0
                }
            }

            impl Not for $name {
                type Output = Self;
                fn not(self) -> Self::Output {
                    $name::new(!self.raw)
                }
            }

            impl BitAnd for $name {
                type Output = Self;
                fn bitand(self, rhs: Self) -> Self::Output {
                    $name::new(self.raw & rhs.raw)
                }
            }

            impl BitOr for $name {
                type Output = Self;
                fn bitor(self, rhs: Self) -> Self::Output {
                    $name::new(self.raw | rhs.raw)
                }
            }

            impl Shr<usize> for $name {
                type Output = Self;
                fn shr(self, rhs: usize) -> Self::Output {
                    $name::new(self.raw >> rhs)
                }
            }


            impl Shl<usize> for $name {
                type Output = Self;
                fn shl(self, rhs: usize) -> Self::Output {
                    $name::new(self.raw << rhs)
                }
            }
        }
    }
}


impl_reg_type!{B8, u8, 1}
impl_reg_type!{B16, u16, 2}
impl_reg_type!{B24, u32, 3}
impl_reg_type!{B32, u32, 4}


impl From<B8::B8> for B16::B16 {
    fn from(value: B8::B8) -> Self {
        B16::B16::new(value.raw() as u16)
    }
}

impl From<B8::B8> for B24::B24 {
    fn from(value: B8::B8) -> Self {
        B24::B24::new(value.raw() as u32)
    }
}

impl From<B8::B8> for B32::B32 {
    fn from(value: B8::B8) -> Self {
        B32::B32::new(value.raw() as u32)
    }
}